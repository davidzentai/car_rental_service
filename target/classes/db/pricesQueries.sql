CREATE TABLE prices (
    license_id varchar(45) NOT NULL,
    oneday int(11) NOT NULL,
	PRIMARY KEY (license_id)
);

select * from prices;

INSERT INTO prices(license_id,price)
VALUES ('ASD-165', 14000),
('DFK-452', 20000),
('EFG-231', 17500),
('KFD-565', 12000),
('KLF-750', 16000),
('LHF-682', 30000),
('LKH-432', 26000),
('PDF-571', 21000);

alter table prices
CHANGE price price_oer_day int(11) NOT NULL;