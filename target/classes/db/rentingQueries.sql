USE CRS;

CREATE TABLE rentings (
	renting_id int(11) NOT NULL AUTO_INCREMENT,
    username varchar(45) NOT NULL,
    license_id varchar(45) NOT NULL,
    rent_from date NOT NULL,
    rent_to date NOT NULL,
    pickup_location int(5) NOT NULL,
    dropdown_location int(5) NOT NULL,
	PRIMARY KEY (renting_id),
    FOREIGN KEY (username) REFERENCES users (username),
    FOREIGN KEY (license_id) REFERENCES cars (license_id),
    FOREIGN KEY (pickup_location) REFERENCES location (location_id),
    FOREIGN KEY (dropdown_location) REFERENCES location (location_id)
);

select * from rentings order by renting_id desc;	

select * from rentings where license_id = 'KFD-565'
and rent_from > '2019-11-20' order by rent_to asc;

select * from rentings where license_id = 'PDF-571'
and rent_from < '2019-11-25' order by rent_from desc;

select * from rentings where license_id = 'KJS-123'
and (rent_from >= '2019-12-04' or rent_to >= '2019-12-04')
and (rent_to <= '2019-12-09' or rent_from <= '2019-12-09')
and renting_id != 41
order by rent_from desc;

select * from rentings where license_id = 'LHF-682'
and rent_to > '2019-11-20' order by rent_to asc;

select * from cars where license_id
not in(
select license_id from rentings where '2019-11-29' between rent_from and rent_to)
and pickup_location = 0;

select * from cars where license_id
not in(
select license_id from rentings where '2019-11-29' between rent_from and rent_to
or '2019-12-05' between rent_from and rent_to
or rent_from between '2019-11-29' and '2019-12-05');

select * from cars where license_id
not in(
select license_id from rentings where '2019-11-29' between rent_from and rent_to);

select * from rentings where license_id='EFG-231' and rent_to > curdate() and renting_id!='14';

delete from rentings where renting_id='4';

alter table rentings
add column total_price int(11) NOT NULL;

update rentings 
set rent_from='2019-11-19', rent_to='2019-11-20', pickup_location='0', dropdown_location='0'
where renting_id = '12';