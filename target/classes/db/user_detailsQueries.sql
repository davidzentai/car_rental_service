USE crs;

CREATE TABLE user_details (
	username varchar(45) NOT NULL,
    firstname varchar(45) NOT NULL,
    lastname varchar(45) NOT NULL,
    phonenumber varchar(45) NOT NULL,
    FOREIGN KEY (username) REFERENCES users (username));
    
SELECT * FROM crs.user_details;