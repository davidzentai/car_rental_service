CREATE TABLE vehicles (
	license_id varchar(45) NOT NULL,
    make varchar(45) NOT NULL,
    model varchar(45) NOT NULL,
    seats int(11) NOT NULL,
    manual_gearbox TINYINT NOT NULL DEFAULT 1,
    doors int(11) NOT NULL,
    category varchar(20) NOT NULL,
	PRIMARY KEY (license_id));
    
select * from cars;

select distinct(category) from cars;

SELECT price_per_day FROM CARS WHERE LICENSE_ID='DFK-452';

ALTER TABLE cars
CHANGE availble available TINYINT NOT NULL DEFAULT 1;

alter table cars
add column price_per_day int(11) NOT NULL,
add column availble TINYINT NOT NULL DEFAULT 1;

RENAME TABLE vehicles TO cars;