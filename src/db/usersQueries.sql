CREATE  TABLE users (
  username VARCHAR(45) NOT NULL ,
  password VARCHAR(45) NOT NULL ,
  enabled TINYINT NOT NULL DEFAULT 1 ,
  PRIMARY KEY (username));
  
SELECT * FROM crs.USERS;

UPDATE USERS
SET password = '$2y$10$kmQN9/sV1wW8jKzejdVYGezHg6vx4Zt3xBA/vAu41g62GUbSHB.kq'
WHERE  password = 'admin';

INSERT INTO users(username,password,enabled)
VALUES ('admin','admin', true);
INSERT INTO users(username,password,enabled)
VALUES ('user','user', true);

INSERT INTO user_roles (username, role)
VALUES ('admin', 'ROLE_USER');
INSERT INTO user_roles (username, role)
VALUES ('admin', 'ROLE_ADMIN');
INSERT INTO user_roles (username, role)
VALUES ('user', 'ROLE_USER');

select username, password, enabled from users where username='admin';