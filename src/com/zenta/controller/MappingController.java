package com.zenta.controller;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.zenta.crs.dao.CarsDAO;
import com.zenta.crs.dao.LocationDAO;
import com.zenta.crs.dao.RentingsDAO;
import com.zenta.crs.dao.UserDAO;
import com.zenta.crs.model.Car;
import com.zenta.crs.model.Location;
import com.zenta.crs.model.Rent;
import com.zenta.crs.model.User;

@Controller
public class MappingController {

    @RequestMapping("/403")
    public ModelAndView error403() {
        return new ModelAndView("403");
    }

    @RequestMapping("/404")
    public ModelAndView error404() {
        return new ModelAndView("404");
    }
	
	@RequestMapping("/aboutUs")
    public ModelAndView aboutUs() {
        return new ModelAndView("aboutUs");
    }
    
    @RequestMapping(value = "/admin**", method = RequestMethod.GET)
    public ModelAndView adminPage() {
        return new ModelAndView("admin");
    }
    
    @RequestMapping("/adminCarList")
    public ModelAndView adminCarList(HttpServletRequest request,
            @RequestParam(value = "msg", required = false) String msg,
            @RequestParam(value = "error", required = false) String error) {
        ModelAndView model = new ModelAndView();
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("/resource/Spring-Module.xml");
        CarsDAO carsDAO = (CarsDAO) context.getBean("carsDAO");
        ArrayList<Car> cars = carsDAO.listAll();
        
        model.addObject("cars", cars);
        
        if(msg != null) {
            if(msg.equals("053")) {
                model.addObject("msg", "Sikeres módosítás");
            }
            if(msg.equals("054")) {
                model.addObject("msg", "Az autót sikeresen hozzáadtuk az adatbázishoz");
            }
            if(msg.equals("055")) {
                model.addObject("msg", "Az autót töröltük az adatbázisból");
            }
        }
        context.close();
        return model;
    }
    
    @RequestMapping("/adminNewCar")
    public ModelAndView adminNewCar(HttpServletRequest request,
            @RequestParam(value = "msg", required = false) String msg,
            @RequestParam(value = "error", required = false) String error) {
        ModelAndView model = new ModelAndView();   

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("/resource/Spring-Module.xml");
        CarsDAO carsDAO = (CarsDAO) context.getBean("carsDAO");
        ArrayList<String> categories = carsDAO.getCategories();
        
        model.addObject("categories", categories);
        
        context.close();
        return model;
    }
    
    @RequestMapping("/adminRentList")
    public ModelAndView adminRentList(HttpServletRequest request,
            @RequestParam(value = "msg", required = false) String msg,
            @RequestParam(value = "error", required = false) String error) {
        ModelAndView model = new ModelAndView();
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("/resource/Spring-Module.xml");
        RentingsDAO rentingsDAO = (RentingsDAO) context.getBean("rentingsDAO");
        LocationDAO locationDAO = (LocationDAO) context.getBean("locationDAO");
        
        ArrayList<Rent> rents = rentingsDAO.listActiveRents();
        ArrayList<String> pickupLocations = new ArrayList<>();
        ArrayList<String> dropdownLocation = new ArrayList<>();
        
        for (Rent rent : rents) {
            pickupLocations.add(locationDAO.getLocationById(rent.getPickup_location()).getName());
            dropdownLocation.add(locationDAO.getLocationById(rent.getDropdown_location()).getName());
        }
        
        model.addObject("pickupLocations", pickupLocations);
        model.addObject("dropdownLocation", dropdownLocation);
        model.addObject("rents", rents);
        
        if(msg != null) {
            if(msg.equals("056")) {
                model.addObject("msg", "A foglalást sikeresen töröltük az adatbázisból");
            }
        }
        context.close();
        return model;
    }
    
    @RequestMapping("/car")
    public ModelAndView carPage(HttpServletRequest request, 
            @RequestParam(value = "error", required = false) String error) {
        ModelAndView model = new ModelAndView();
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("/resource/Spring-Module.xml");
        		
        /* GET CAR BY LICENSE ID */
        CarsDAO carsDAO = (CarsDAO) context.getBean("carsDAO");
        String license_id = request.getParameter("rent");
        Car car = carsDAO.getCarByLicenseId(license_id);        
        if (car.isAvailable()) {
            model.addObject("car", car);
        } else {
            /* IF THE CAR IS NOT AVAILABLE */
            context.close();
            model.setViewName("redirect:/404");
            return model;
        }
        
        /* GET AVAILABLE LOCATIONS */
        LocationDAO locationDAO = (LocationDAO) context.getBean("locationDAO");
        ArrayList<Location> locations = locationDAO.listLocations();
        model.addObject("locations", locations);
        
        /* GET RESERVED DATES */
        RentingsDAO rentingsDAO = (RentingsDAO) context.getBean("rentingsDAO");
        ArrayList<Date> reservedDates = new ArrayList<>();
        
        /* MODIFICATION PAGE */
        if (request.getParameter("modification") != null) {
        	//System.out.println("RENTINGID: " +  request.getParameter("modify"));
        	if(rentingsDAO.isRestricted(request.getParameter("modify"), request.getParameter("email"))) {
                context.close();
                model.setViewName("redirect:/403");
                return model;
        	}
            reservedDates = rentingsDAO.getReservedDatesByLicenceId(license_id, request.getParameter("modify"));
            model.addObject("modification", true);
            Rent rent = rentingsDAO.getRent(request.getParameter("modify"));        //modify.value = rentingId
            model.addObject("pickupLocation", locationDAO.getLocationById(rent.getPickup_location()).getName());
            model.addObject("dropdownLocation", locationDAO.getLocationById(rent.getDropdown_location()).getName());
            model.addObject("rent", rent);
        } 
        
		/* CREATION PAGE */
        else {
            request.setAttribute("rentFrom", request.getParameter("rentFrom"));
            request.setAttribute("rentTo", request.getParameter("rentTo"));
            request.setAttribute("fromLocation", request.getParameter("fromLocation"));
            request.setAttribute("toLocation", request.getParameter("toLocation"));
            reservedDates = rentingsDAO.getReservedDatesByLicenceId(license_id);
        }
        model.addObject("reservedDates", reservedDates);
        
        /* ERROR HANDLING */
        if (error != null) {
        	errorMessage(model, error);
        }
        
        context.close();
        return model;
    }
    
    @RequestMapping("/carhistory")
    public ModelAndView rentingHistory(HttpServletRequest request,
            @RequestParam(value = "error", required = false) String error,
            @RequestParam(value = "msg", required = false) String msg) {
    	
        ModelAndView model = new ModelAndView();
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("/resource/Spring-Module.xml");
        RentingsDAO rentingsDAO = (RentingsDAO) context.getBean("rentingsDAO");
        ArrayList<Rent> rents = rentingsDAO.listRents(request.getRemoteUser());
        model.addObject("rents", rents);
        
		/* GET RENT'S DETAILS FROM OTHER TABLES */
        CarsDAO carsDAO = (CarsDAO) context.getBean("carsDAO");
        LocationDAO locationDAO = (LocationDAO) context.getBean("locationDAO");
        ArrayList<Car> cars = new ArrayList<>();
        ArrayList<String> pickupLocations = new ArrayList<>();
        ArrayList<String> dropdownLocation = new ArrayList<>();
        for (Rent rent : rents) {
            cars.add(carsDAO.getCarByLicenseId(rent.getLicense_id()));
            pickupLocations.add(locationDAO.getLocationById(rent.getPickup_location()).getName());
            dropdownLocation.add(locationDAO.getLocationById(rent.getDropdown_location()).getName());
        }
        model.addObject("cars", cars);
        model.addObject("pickupLocations", pickupLocations);
        model.addObject("dropdownLocation", dropdownLocation);
        
        if(msg != null) {
            if(msg.equals("050")) {
                model.addObject("msg", "Sikeresen töröltük a kért kölcsönzést");
            }
            else if(msg.equals("051")) {
                model.addObject("msg", "Sikeresen létrehoztuk az új kölcsönzést");
            }
            else if(msg.equals("052")) {
                model.addObject("msg", "Sikeresen módosítottuk a kölcsönzést");
            }
        }

        /* ERROR HANDLING */
        if (error != null) {
        	errorMessage(model, error);
        }
        
        context.close();
        return model;
    }

    @RequestMapping(value="/contact")
    public ModelAndView contactPage() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("/resource/Spring-Module.xml");
        LocationDAO locationDAO = (LocationDAO) context.getBean("locationDAO");
        ArrayList<Location> locations = locationDAO.listLocations();
        context.close();
        return new ModelAndView("contact", "locations", locations);
    }
    
    @RequestMapping("/createCar")
    public ModelAndView createCar(HttpServletRequest request) {
        ModelAndView model = new ModelAndView();
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("/resource/Spring-Module.xml");
        CarsDAO carsDAO = (CarsDAO) context.getBean("carsDAO");
        
        Car car = new Car(
                request.getParameter("license_id"), 
                request.getParameter("make"), 
                request.getParameter("model"), 
                Integer.parseInt(request.getParameter("seats")),
                getCheckboxValue(request.getParameter("manual_gearbox")), 
                Integer.parseInt(request.getParameter("doors")), 
                request.getParameter("category"), 
                request.getParameter("image").substring(0, request.getParameter("image").length()-4), 
                Integer.parseInt(request.getParameter("year")), 
                Integer.parseInt(request.getParameter("pricePerDay")),
                getCheckboxValue(request.getParameter("available")));
                
        carsDAO.createCar(car);
        
        car.saveImage(request.getParameter("imageFile"), request.getParameter("image"));
        
        model.addObject("msg", "054");
        model.setViewName("redirect:/adminCarList");
        
        context.close();
        return model;
    }

    @RequestMapping(value = "/deleteRent")
    public ModelAndView deleteRent(HttpServletRequest request) {
        ModelAndView model = new ModelAndView();
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("/resource/Spring-Module.xml");
        RentingsDAO rentingsDAO = (RentingsDAO) context.getBean("rentingsDAO");
        
    	if(rentingsDAO.isRestricted(request.getParameter("delete"), request.getParameter("email"))) {
            context.close();
            model.setViewName("redirect:/403");
            return model;
        }
    	
        rentingsDAO.deleteRent(request.getParameter("delete"));
        
        context.close();
        model.addObject("msg", "050");
        model.setViewName("redirect:/carhistory");
        return model;
    }

    @RequestMapping(value = "/deleteRentAdmin")
    public ModelAndView deleteRentAdmin(HttpServletRequest request) {
        ModelAndView model = new ModelAndView();
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("/resource/Spring-Module.xml");
        RentingsDAO rentingsDAO = (RentingsDAO) context.getBean("rentingsDAO");
        rentingsDAO.deleteRent(request.getParameter("delete"));
        
        context.close();
        model.addObject("msg", "056");
        model.setViewName("redirect:/adminRentList");
        return model;
    }
    
    @RequestMapping("/deleteCar")
    public ModelAndView deleteCar(HttpServletRequest request) {
        ModelAndView model = new ModelAndView();
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("/resource/Spring-Module.xml");
        CarsDAO carsDAO = (CarsDAO) context.getBean("carsDAO");
        carsDAO.deleteCar(request.getParameter("delete"));
        
        model.addObject("msg", "055");
        model.setViewName("redirect:/adminCarList");
        
        context.close();
        return model;        
    }

    @RequestMapping("/index")
    public ModelAndView indexPage(HttpServletRequest request) {
        ModelAndView model = new ModelAndView();
        
	    ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("/resource/Spring-Module.xml");
        CarsDAO carsDAO = (CarsDAO) context.getBean("carsDAO");
        ArrayList<Car> cars = carsDAO.listAll();
        
        LocationDAO locationDAO = (LocationDAO) context.getBean("locationDAO");
        ArrayList<Location> locations = locationDAO.listLocations();
        model.addObject("locations", locations);
        
        /* SEARCHBOX FILTERS */
        RentingsDAO rentingsDAO = (RentingsDAO) context.getBean("rentingsDAO");
        if(request.getParameter("categories") != null) {
            String[] categories = request.getParameter("categories").split(",");
            for (int i = 0; i < categories.length; i++) {
                //System.out.println("CATEGORIES: " + categories[i]);
                model.addObject("categories"+i, categories[i]);
    		}
        }
        
		/* IN CASE OF FILTERING HAPPENED */
        if (request.getParameter("rentFrom") != null) {
			/* IN CASE OF RENTFROM AND RENTTO ARE NOT EMPTY */
            if(!request.getParameter("rentFrom").equals("") && !request.getParameter("rentTo").equals("")) {
                Date rentFrom = adjustDateFormat(request.getParameter("rentFrom"));
                Date rentTo = adjustDateFormat(request.getParameter("rentTo"));
                ArrayList<Rent> rents = rentingsDAO.getRentsByDates(rentFrom, rentTo);
                deleteCarsWhichInRents(cars, rents);
                model.addObject("rentFrom", rentFrom);
                model.addObject("rentTo", rentTo);
				/* IN CASE OF LOACTIONS ARE SELECTED */
                if(!request.getParameter("fromLocation").equals("") && !request.getParameter("toLocation").equals("")) {
                    deleteCarsByLocation(cars, Integer.parseInt(request.getParameter("fromLocation")), Integer.parseInt(request.getParameter("toLocation")), rentFrom, rentTo);
                    model.addObject("fromLocation", Integer.parseInt(request.getParameter("fromLocation")));
                    model.addObject("toLocation", Integer.parseInt(request.getParameter("toLocation")));
                }
            } else {
				/* IN CASE OF RENTFROM IS NOT EMPTY */
                if(!request.getParameter("rentFrom").equals("")) {
                    Date rentFrom = adjustDateFormat(request.getParameter("rentFrom"));
                    ArrayList<Rent> rents = rentingsDAO.getRentsByDate(rentFrom);
                    deleteCarsWhichInRents(cars, rents);
                    model.addObject("rentFrom", rentFrom);
    				/* IN CASE OF FROMLOACTION IS SELECTED */
                    if(!request.getParameter("fromLocation").equals("")) {
                        deleteCarsByLocation(cars, Integer.parseInt(request.getParameter("fromLocation")), null, rentFrom, null);
                        model.addObject("fromLocation", Integer.parseInt(request.getParameter("fromLocation")));
                    }
                }
				/* IN CASE OF RENTTO IS NOT EMPTY */
                if(!request.getParameter("rentTo").equals("")) {
                    Date rentTo = adjustDateFormat(request.getParameter("rentTo"));
                    ArrayList<Rent> rents = rentingsDAO.getRentsByDate(rentTo);
                    deleteCarsWhichInRents(cars, rents);
                    model.addObject("rentTo", rentTo);
    				/* IN CASE OF TOLOACTION IS SELECTED */
                    if(!request.getParameter("toLocation").equals("")) {
                    	deleteCarsByLocation(cars, null, Integer.parseInt(request.getParameter("toLocation")), null, rentTo);
                        model.addObject("toLocation", Integer.parseInt(request.getParameter("toLocation")));
                    }
                }
            }
        }
        model.addObject("cars", cars);
        
        context.close();
        return model;
    }
	
	@RequestMapping(value = "/info", method = RequestMethod.GET)
    public ModelAndView info() {
        return new ModelAndView("info");
    }
	
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login(
        @RequestParam(value = "error", required = false) String error,
        @RequestParam(value = "logout", required = false) String logout) {

        ModelAndView model = new ModelAndView();
        if (error != null) {
            model.addObject("error", "Hibás e-mail cím vagy jelszó!");
        }

        if (logout != null) {
            model.addObject("msg", "Sikeres kijelentkezés.");
        }
        model.setViewName("login");

        return model;
    }
    
    @RequestMapping("/modifyCar")
    public ModelAndView modifyCar(HttpServletRequest request) {
        ModelAndView model = new ModelAndView();
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("/resource/Spring-Module.xml");
        CarsDAO carsDAO = (CarsDAO) context.getBean("carsDAO");
        Car car = carsDAO.getCarByLicenseId(request.getParameter("save"));
        if(!request.getParameter("image").equals("")) {
            car.setImage(request.getParameter("image").substring(0, request.getParameter("image").length()-4));
            car.saveImage(request.getParameter("imageFile"), request.getParameter("image"));
        }
        car.setPricePerDay(Integer.parseInt(request.getParameter("price")));
        if(request.getParameter("available") != null) {
            //System.out.println("AVAILABLE: TRUE");
            car.setAvailable(true);
        } else {
            //System.out.println("AVAILABLE: FALSE");
            car.setAvailable(false);
        }
        //car.setAvailable(Boolean.parseBoolean(request.getParameter("available")));
        carsDAO.modifyCar(car);
        
        model.addObject("msg", "053");
        model.setViewName("redirect:/adminCarList");
        
        context.close();
        return model;        
    }
    
    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public ModelAndView registrationPage( @RequestParam(value = "error", required = false) String error) {
        return new ModelAndView("registration");
    }
    
    @RequestMapping(value = "/registrationSuccess", method = RequestMethod.POST)
    public ModelAndView registrationSuccess(HttpServletRequest request) {
        
        ModelAndView model = new ModelAndView();    
        
        User user = new User();
        user.setUsername(request.getParameter("email"));
        user.setFirstname(request.getParameter("firstname"));
        user.setLastname(request.getParameter("lastname"));
        user.setPhonenumber(request.getParameter("phonenumber"));
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode(request.getParameter("password"));
        user.setPassword(hashedPassword);
        
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("/resource/Spring-Module.xml");
        UserDAO userDAO = (UserDAO) context.getBean("userDAO");
        if(userDAO.isEmailAlreadyUse(user.getUsername())) {
            context.close();
            model.addObject("error", "A megadott e-mailcímmel már van létrehozva fiók.");
            model.setViewName("registration");
            return model;
        } else {
            userDAO.insert(user);
            context.close();
        }
        
        model.setViewName("registrationSuccess");
        return model;
    }
    
    @RequestMapping(value = "/rentingSuccess", params = "confirmModification")
    public ModelAndView confrirmModification(HttpServletRequest request) throws ParseException {
        ModelAndView model = new ModelAndView();
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("/resource/Spring-Module.xml");
        RentingsDAO rentingsDAO = (RentingsDAO) context.getBean("rentingsDAO");
        Rent rent = rentingsDAO.getRent(request.getParameter("confirmModification"));
        rent.setRent_from(adjustDateFormat(request.getParameter("rentFrom")));
        rent.setRent_to(adjustDateFormat(request.getParameter("rentTo")));
        rent.setPickup_location(Integer.parseInt(request.getParameter("pickupLocation")));
        rent.setDropdown_location(Integer.parseInt(request.getParameter("dropdownLocation")));

        CarsDAO carsDAO = (CarsDAO) context.getBean("carsDAO");
        Car car = carsDAO.getCarByLicenseId(rent.getLicense_id());
        rent.setTotalPrice(rent.calculatePrice(car.getPricePerDay()));
        
        ModelAndView validModel = backendValidationForRenting(rent, true);
        if (validModel != null) {
            validModel.addObject("modification", true);
            context.close();
            return validModel;
        } else {
	        rentingsDAO.modifyRent(rent);
	        model.addObject("msg", "052");
	        model.setViewName("redirect:/carhistory");
            context.close();
	        return model;
        }
    }
    
    @RequestMapping(value = "/rentingSuccess", params = "confirmRenting")
    public ModelAndView rentingSuccess(HttpServletRequest request, 
            @RequestParam(value = "error", required = false) String error) throws ParseException {
        ModelAndView model = new ModelAndView();

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("/resource/Spring-Module.xml");

        CarsDAO carsDAO = (CarsDAO) context.getBean("carsDAO");     
        RentingsDAO rentingsDAO = (RentingsDAO) context.getBean("rentingsDAO");
        Rent rent = new Rent(
                request.getParameter("email"), 
                request.getParameter("confirmRenting"), 
                request.getParameter("rentFrom"), 
                request.getParameter("rentTo"), 
                Integer.parseInt(request.getParameter("pickupLocation")), 
                Integer.parseInt(request.getParameter("dropdownLocation")),
                carsDAO.getCarByLicenseId(request.getParameter("confirmRenting")).getPricePerDay());
        
        ModelAndView validModel = backendValidationForRenting(rent, false);
        if (validModel != null) {
            context.close();
            return validModel;
        } else {
            rentingsDAO.createRent(rent);
            model.addObject("msg", "051");
            model.setViewName("redirect:/carhistory");
            context.close();
            return model;
        }
    }
    
	/* Controller segédfüggvények */
    
    public Date adjustDateFormat(String strDate) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date utilDate = sdf.parse(strDate);
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
            return sqlDate;
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
    
    public ModelAndView backendValidationForRenting(Rent rent, boolean isModification) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("/resource/Spring-Module.xml");

        CarsDAO carsDAO = (CarsDAO) context.getBean("carsDAO");
        LocationDAO locationDAO = (LocationDAO) context.getBean("locationDAO");
        RentingsDAO rentingsDAO = (RentingsDAO) context.getBean("rentingsDAO");
    	
    	/* BACKEND CHECK FOR RENT_TO IS GREATER THAN RENT_FROM */
        HashMap<String, String> errorDetails = new HashMap<String, String>();
        if (!isModification) {
            errorDetails.put("rent", rent.getLicense_id());
            errorDetails.put("rentFrom", rent.getRent_from().toString());
            errorDetails.put("rentTo", rent.getRent_to().toString());
            errorDetails.put("fromLocation", String.valueOf(rent.getPickup_location()));
            errorDetails.put("toLocation", String.valueOf(rent.getDropdown_location()));
        }
        if (rent.getRent_from().compareTo(rent.getRent_to()) > 0) {
            context.close();
    		if (isModification) return errorHandler("001", errorDetails, "redirect:/carhistory");
            return errorHandler("001", errorDetails, "redirect:/car");
        }

		/* BACKEND CHECK FOR RENTING DATES AND LOCATIONS */
        Date currentDate = new Date(Calendar.getInstance().getTime().getTime());
        if( currentDate.after(rent.getRent_from())) {
    		context.close();
    		if (isModification) return errorHandler("014", errorDetails, "redirect:/carhistory");
    		return errorHandler("014", errorDetails, "redirect:/car");
        }
        
    	Rent prevRent = rentingsDAO.getPreviousRent(rent.getLicense_id(), rent.getRent_from());
    	Rent nextRent = rentingsDAO.getNextRent(rent.getLicense_id(), rent.getRent_to());
    	Rent overlappingRent = new Rent();
    	if (isModification) {
        	overlappingRent = rentingsDAO.getOverlappingRentsWithExclude(rent.getLicense_id(), rent.getRent_from(), rent.getRent_to(), rent.getRenting_id());
    	} else {
        	overlappingRent = rentingsDAO.getOverlappingRents(rent.getLicense_id(), rent.getRent_from(), rent.getRent_to());
    	}
    	if (prevRent != null) {
    		if(prevRent.getRent_to().compareTo(rent.getRent_from()) > 0) {
                context.close();
        		if (isModification) return errorHandler("009", errorDetails, "redirect:/carhistory");
                return errorHandler("004", errorDetails, "redirect:/car");
    		}
    		if(prevRent.getDropdown_location() != rent.getPickup_location()) {
                context.close();
        		if (isModification) return errorHandler("007", errorDetails, "redirect:/carhistory");
                return errorHandler("002", errorDetails, "redirect:/car");
            }
        }
        if (nextRent != null) {
        	if(nextRent.getRent_from().compareTo(rent.getRent_to()) < 0) {
                context.close();
        		if (isModification) return errorHandler("010", errorDetails, "redirect:/carhistory");
                return errorHandler("005", errorDetails, "redirect:/car");
        	}
            if(nextRent.getPickup_location() != rent.getDropdown_location()) {
                context.close();
        		if (isModification) return errorHandler("008", errorDetails, "redirect:/carhistory");
                return errorHandler("003", errorDetails, "redirect:/car");
            }
        }
    	if (overlappingRent != null) {
    		context.close();
    		if (isModification) return errorHandler("011", errorDetails, "redirect:/carhistory");
    		return errorHandler("006", errorDetails, "redirect:/car");
    	}

        /* BACKEND CHECK FOR LOCATION'S STALL */
    	ArrayList<String> licenseIds = carsDAO.getLicenseIds();
    	ArrayList<Rent> rentsInToLocation = new ArrayList<>();
    	for (String licenseId : licenseIds) {
    	    if (!licenseId.equals(rent.getLicense_id())) {
                Rent temp = rentingsDAO.getPreviousRent(licenseId, rent.getRent_to());
                if (temp != null) {
                    if (temp.getDropdown_location() == rent.getDropdown_location()) {
                        rentsInToLocation.add(temp);
                        System.out.println("AUTÓ: " + temp.getLicense_id() + " - " + temp.getRent_to() + " - " + temp.getDropdown_location());
                    }
                }
    	    }
        }
    	System.out.println("RENTSINLOCATION.SIZE(): " + rentsInToLocation.size() + ", " + locationDAO.getStalls(rent.getDropdown_location()));
    	if (rentsInToLocation.size() >= locationDAO.getStalls(rent.getDropdown_location())) {
            context.close();
            if (isModification) return errorHandler("013", errorDetails, "redirect:/carhistory");
            return errorHandler("012", errorDetails, "redirect:/car");
    	}
            
    	context.close();
    	return null;
    }

    public void deleteCarsByLocation(ArrayList<Car> cars, Integer fromLocation, Integer toLocation, Date fromDate, Date toDate) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("/resource/Spring-Module.xml");
        RentingsDAO rentingsDAO = (RentingsDAO) context.getBean("rentingsDAO");
        ArrayList<Car> tmp = new ArrayList<>();
        if (fromDate != null) {
            for (Car car : cars) {
                Rent rent = rentingsDAO.getPreviousRent(car.getLicense_id(), fromDate);
                if (rent == null) {
                    //System.out.println("NINCS ELÕZÕ RENDELÉS A JÁRMÛHÖZ: " + car.getLicense_id());
                } else {
                    if(rent.getDropdown_location() != fromLocation) {
                        tmp.add(car);
                    }
                }
            }
            for (Car car : tmp) {
                cars.remove(car);
            }
        }
        if (toDate != null) {
            for (Car car : cars) {
                Rent rent = rentingsDAO.getNextRent(car.getLicense_id(), toDate);
                if (rent == null) {
                    //System.out.println("NINCS KÖVETKEZÕ RENDELÉS A JÁRMÛHÖZ: " + car.getLicense_id());
                } else {
                    if(rent.getPickup_location() != toLocation) {
                        tmp.add(car);
                    }
                }
            }
            for (Car car : tmp) {
                cars.remove(car);
            }
        }
        context.close();
    }
    
    public void deleteCarsWhichInRents(ArrayList<Car> cars, ArrayList<Rent> rents) {
        
        for (Rent rent : rents) {
            Car tmp = null;
            for (Car car : cars) {
                if (rent.getLicense_id().equals(car.getLicense_id())) {
                    tmp = car;
                    break;
                }
            }
            cars.remove(tmp);
        }
    }

    public ModelAndView errorHandler(String errno, HashMap<String, String> details, String address) {
    	ModelAndView model = new ModelAndView();
    	model.addObject("error", errno);
    	for (Map.Entry<String, String> detail : details.entrySet()) {
        	model.addObject(detail.getKey(), detail.getValue());
    	}
    	model.setViewName(address);
    	return model;
    }
    
    public void errorMessage(ModelAndView model, String error) {
        if(error.equals("001")) {
            model.addObject("error", "A leadás dátuma megelõzi a kölcsönzés kezdeti dátumát.");
        }
        else if(error.equals("002")) {
            model.addObject("error", "A kölcsönzés létrehozása nem lehetséges a megadott kölcsönzési telephellyel.");
        }
        else if(error.equals("003")) {
            model.addObject("error", "A kölcsönzés létrehozása nem lehetséges a megadott leadási telephellyel.");
        }
        else if(error.equals("004")) {
            model.addObject("error", "A kölcsönzés létrehozása nem lehetséges a megadott kezdési dátummal.");
        }
        else if(error.equals("005")) {
            model.addObject("error", "A kölcsönzés létrehozása nem lehetséges a megadott leadási dátummal.");
        }
        else if(error.equals("006")) {
            model.addObject("error", "A kölcsönzés létrehozása nem lehetséges: egy másik rendeléssel átfedésben van.");
        }
        else if(error.equals("007")) {
            model.addObject("error", "A kölcsönzés módosítása nem lehetséges a megadott kölcsönzési telephellyel.");
        }
        else if(error.equals("008")) {
            model.addObject("error", "A kölcsönzés módosítása nem lehetséges a megadott leadási telephellyel.");
        }
        else if(error.equals("009")) {
            model.addObject("error", "A kölcsönzés módosítása nem lehetséges a megadott kezdési dátummal.");
        }
        else if(error.equals("010")) {
            model.addObject("error", "A kölcsönzés módosítása nem lehetséges a megadott leadási dátummal.");
        }
        else if(error.equals("011")) {
            model.addObject("error", "A kölcsönzés módosítása nem lehetséges: egy másik rendeléssel átfedésben van.");
        }
        else if(error.equals("012")) {
            model.addObject("error", "A kölcsönzés létrehozása nem lehetséges: a kiválasztott telephely megtelt.");
        }
        else if(error.equals("013")) {
            model.addObject("error", "A kölcsönzés módosítása nem lehetséges: a kiválasztott telephely megtelt.");
        }
        else if(error.equals("014")) {
            model.addObject("error", "A kölcsönzés dátuma nem lehet a múltban.");
        }
    }
    
    public boolean getCheckboxValue(String param) {
        if(param != null) {
            return true;
        } else {
            return false;
        }
    }
}
