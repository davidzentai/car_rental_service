package com.zenta.crs.model;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;

public class Rent {
    
    private int renting_id;
    private String username;
    private String license_id;
    private Date rent_from;;
    private Date rent_to;
    private int pickup_location;
    private int dropdown_location;
    private int totalPrice;
    
    public Rent() {}

    public Rent(String username, String license_id, String rent_from, String rent_to, int pickup_location, int dropdown_location, int oneDayPrice) {
        this.username = username;
        this.license_id = license_id;
        this.setRent_from(rent_from);
        this.setRent_to(rent_to);
        this.pickup_location = pickup_location;
        this.dropdown_location = dropdown_location;
        this.setTotalPrice(this.calculatePrice(oneDayPrice));
    }
    
    public Rent(int renting_id, String username, String license_id, Date rent_from, Date rent_to, int pickup_location, int dropdown_location, int totalPrice) {
        this.renting_id = renting_id;
        this.username = username;
        this.license_id = license_id;
        this.rent_from = rent_from;
        this.rent_to = rent_to;
        this.pickup_location = pickup_location;
        this.dropdown_location = dropdown_location;
        this.totalPrice = totalPrice;
    }

    public String getLicense_id() {
        return license_id;
    }

    public void setLicense_id(String license_id) {
        this.license_id = license_id;
    }

    public int getRenting_id() {
        return renting_id;
    }

    public void setRenting_id(int renting_id) {
        this.renting_id = renting_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getRent_from() {
        return rent_from;
    }

    public void setRent_from(Date rent_from) {
        this.rent_from = rent_from;
    }
    
    public void setRent_from(String rent_from_str) {
        try {
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date date = sdf1.parse(rent_from_str);
            java.sql.Date rent_from = new java.sql.Date(date.getTime());
            this.rent_from = rent_from;
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public Date getRent_to() {
        return rent_to;
    }

    public void setRent_to(Date rent_to) {
        this.rent_to = rent_to;
    }
    
    public void setRent_to(String rent_to_str) {
        try {
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date date = sdf1.parse(rent_to_str);
            java.sql.Date rent_to = new java.sql.Date(date.getTime());
            this.rent_to = rent_to;
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public int getPickup_location() {
        return pickup_location;
    }

    public void setPickup_location(int pickup_location) {
        this.pickup_location = pickup_location;
    }

    public int getDropdown_location() {
        return dropdown_location;
    }

    public void setDropdown_location(int dropdown_location) {
        this.dropdown_location = dropdown_location;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }
    
    public int calculatePrice(int oneDayPrice) {
        return oneDayPrice * getRentingDays();
    }
    
    public int getRentingDays() {
        long diff = this.getRent_to().getTime() - this.getRent_from().getTime();
        int days = (int)TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) + 1;
        return days;
    }
}
