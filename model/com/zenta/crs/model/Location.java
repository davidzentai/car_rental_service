package com.zenta.crs.model;

public class Location {

    private int locationId;
    private String name;
    private String address;
    private String GPS;
    private int stalls;
    private String phone;
    private String email;
    
    public Location(int locationId, String name, String address, String GPS, int stalls, String phone, String email) {
        super();
        this.locationId = locationId;
        this.name = name;
        this.address = address;
        this.GPS = GPS;
        this.stalls = stalls;
        this.phone = phone;
        this.email = email;
    }
    public int getLocationId() {
        return locationId;
    }
    public String getName() {
        return name;
    }
    public String getAddress() {
        return address;
    }
    public String getGPS() {
        return GPS;
    }
    public int getStalls() {
        return stalls;
    }
    public String getPhone() {
        return phone;
    }
    public String getEmail() {
        return email;
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("LOCATIONID: " + this.getLocationId() + "\n");
        sb.append("NAME: " + this.getName() + "\n");
        sb.append("ADDRESS: " + this.getAddress() + "\n");
        sb.append("GPS: " + this.getGPS() + "\n");
        sb.append("STALLS: " + this.getStalls() + "\n");
        sb.append("PHONE: " + this.getPhone() + "\n");
        sb.append("EMAIL: " + this.getEmail() + "\n");
        return sb.toString();
    }
}
