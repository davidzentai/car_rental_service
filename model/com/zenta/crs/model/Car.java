package com.zenta.crs.model;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import sun.misc.*;

public class Car {
    private String license_id;
    private String make;
    private String model;
    private int seats;
    private boolean manual_gearbox;
    private int doors;
    private String category;
    private String image;
    private int year;
    private int pricePerDay;
    private boolean available;
    
    public Car(String license_id, String make, String model, int seats, boolean manual_gearbox, int doors, String category, String image, int year, int pricePerDay, boolean available) {
        this.license_id = license_id;
        this.make = make;
        this.model = model;
        this.seats = seats;
        this.manual_gearbox = manual_gearbox;
        this.doors = doors;
        this.category = category;
        this.image = image;
        this.year = year;
        this.pricePerDay = pricePerDay;
        this.available = available;
    }
    public int getYear() {
        return year;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public String getLicense_id() {
        return license_id;
    }
    public void setLicense_id(String license_id) {
        this.license_id = license_id;
    }
    public String getMake() {
        return make;
    }
    public void setMake(String make) {
        this.make = make;
    }
    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }
    public int getSeats() {
        return seats;
    }
    public void setSeats(int seats) {
        this.seats = seats;
    }
    public boolean isManual_gearbox() {
        return manual_gearbox;
    }
    public void setManual_gearbox(boolean manual_gearbox) {
        this.manual_gearbox = manual_gearbox;
    }
    public int getDoors() {
        return doors;
    }
    public void setDoors(int doors) {
        this.doors = doors;
    }
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }
    public int getPricePerDay() {
        return pricePerDay;
    }
    public void setPricePerDay(int pricePerDay) {
        this.pricePerDay = pricePerDay;
    }
    public boolean isAvailable() {
        return available;
    }
    public void setAvailable(boolean available) {
        this.available = available;
    }
    public void saveImage(String imageUrl, String destinationFile) {
        try {
            String[] parts = imageUrl.split(",");
            byte[] btDataFile = new sun.misc.BASE64Decoder().decodeBuffer(parts[1]);
            File of = new File("WebContent/resources/img/" + destinationFile);
            FileOutputStream osf = new FileOutputStream(of);
            
            osf.write(btDataFile);
            osf.flush();
            osf.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
