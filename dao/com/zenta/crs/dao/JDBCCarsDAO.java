package com.zenta.crs.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.sql.DataSource;

import com.zenta.crs.model.Car;

public class JDBCCarsDAO implements CarsDAO {

    private DataSource dataSource;
    
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    
    @Override
    public void createCar(Car car) {
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO `cars` (`license_id`, `make`, `model`, `seats`, `manual_gearbox`, `doors`, `category`, `image_name`, `year`, `price_per_day`, `available`) VALUES ('");
        sb.append(car.getLicense_id());
        sb.append("', '");
        sb.append(car.getMake());
        sb.append("', '");
        sb.append(car.getModel());
        sb.append("', '");
        sb.append(car.getSeats());
        sb.append("', '");
        if(car.isManual_gearbox()) {
            sb.append(1);
        } else {
            sb.append(0);
        }
        sb.append("', '");
        sb.append(car.getDoors());
        sb.append("', '");
        sb.append(car.getCategory());
        sb.append("', '");
        sb.append(car.getImage());
        sb.append("', '");
        sb.append(car.getYear());
        sb.append("', '");
        sb.append(car.getPricePerDay());
        sb.append("', '");
        if(car.isAvailable()) {
            sb.append(1);
        } else {
            sb.append(0);
        }
        sb.append("')");
        writeDB(sb.toString());
    }

    @Override
    public void deleteCar(String licenseId) {
        StringBuilder sb = new StringBuilder();
        sb.append("delete from cars where license_id='");
        sb.append(licenseId);
        sb.append("'");
        String sql = sb.toString();
        Connection conn = null;
        
        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
            
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {}
            }
        }
    }

    @Override
    public Car getCarByLicenseId(String license_id) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM CARS WHERE LICENSE_ID='");
        sb.append(license_id);
        sb.append("'");
        ArrayList<Car> cars = getCarsFromDB(sb.toString());
        return cars.get(0);
    }
    
    @Override
    public int getCarPriceByLicenseId(String licenseId) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT price_per_day FROM CARS WHERE LICENSE_ID='");
        sb.append(licenseId);
        sb.append("'");
        ArrayList<Car> cars = getCarsFromDB(sb.toString());
        return cars.get(0).getPricePerDay();
    }

    @Override
    public ArrayList<Car> getCarsFromDB(String sql) {
        ArrayList<Car> cars = new ArrayList<>();
        Connection conn = null;
        
        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            Car car = null;
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                car = new Car(
                    rs.getString("license_id"),
                    rs.getString("make"), 
                    rs.getString("model"), 
                    rs.getInt("seats"), 
                    rs.getBoolean("manual_gearbox"), 
                    rs.getInt("doors"),
                    rs.getString("category"), 
                    rs.getString("image_name"),
                    rs.getInt("year"),
                    rs.getInt("price_per_day"),
                    rs.getBoolean("available")
                );
                cars.add(car);
            }
            rs.close();
            ps.close();
            return cars;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                try {
                conn.close();
                } catch (SQLException e) {}
            }
        }
    }

    @Override
    public ArrayList<String> getCategories() {
        String sql = "select distinct(category) from cars";
        ArrayList<String> categories = new ArrayList<>();
        Connection conn = null;
        
        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) { 
                categories.add(rs.getString("category"));
            }
            rs.close();
            ps.close();
            return categories;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                try {
                conn.close();
                } catch (SQLException e) {}
            }
        }
    }
    
    @Override
    public ArrayList<String> getLicenseIds() {

        String sql = "select license_id from cars";
        ArrayList<String> licenseIds = new ArrayList<>();
        Connection conn = null;
        
        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) { 
                licenseIds.add(rs.getString("license_id"));
            }
            rs.close();
            ps.close();
            return licenseIds;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                try {
                conn.close();
                } catch (SQLException e) {}
            }
        }
    }

    @Override
    public ArrayList<Car> listAll() {
        String sql = "SELECT * FROM CARS";
        return getCarsFromDB(sql);
    }
    
    @Override
    public void modifyCar(Car car) {
        StringBuilder sb = new StringBuilder();
        sb.append("update cars set image_name='");
        sb.append(car.getImage());
        sb.append("', price_per_day='");
        sb.append(car.getPricePerDay());
        sb.append("', available='");
        if(car.isAvailable()) {
            sb.append(1);
        } else {
            sb.append(0);
        }        
        sb.append("' where license_id='");
        sb.append(car.getLicense_id());
        sb.append("'");
        writeDB(sb.toString());
    }

    @Override
    public ArrayList<Car> searchByParams(ArrayList<String> categories) {
        StringBuilder sb = new StringBuilder();
        if(categories.isEmpty()) {
            sb.append("SELECT * FROM CARS");
        } else {
            sb.append("SELECT * FROM CARS WHERE");
            for (int i = 0; i < categories.size(); i++) {
                sb.append(" CATEGORY='");
                sb.append(categories.get(i));
                sb.append("'");
                if(categories.size() > i+1) {
                    sb.append(" OR");
                }
            }
        }
        return getCarsFromDB(sb.toString());
    }



    @Override
    public void writeDB(String sql) {
        Connection conn = null;
        
        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {}
            }
        }
    }
    
    
}
