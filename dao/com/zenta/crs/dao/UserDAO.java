package com.zenta.crs.dao;

import com.zenta.crs.model.User;;

public interface UserDAO {
    public void insert(User user);
    public boolean isEmailAlreadyUse(String email);
}
