package com.zenta.crs.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.zenta.crs.model.User;

public class JDBCUserDAO implements UserDAO {

    private DataSource dataSource;
    
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    
    @Override
    public boolean isEmailAlreadyUse(String email) {
        String sql = "select username from users where username = ?";
        Connection conn = null;
        
        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, email);
            String username = null;
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                username = rs.getString("username");
                if(username != null) {
                    return true;
                }
            }
            rs.close();
            ps.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                try {
                conn.close();
                } catch (SQLException e) {}
            }
        }        
        return false;
    }



    @Override
    public void insert(User user) {
        
        /* --- USER_DETAILS TABLE --- */
        String sqlUser = "insert into users (username, password) values (?, ?)";
        Connection conn = null;
        
        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sqlUser);
            ps.setString(1, user.getUsername());
            ps.setString(2, user.getPassword());
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
            
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {}
            }
        }
        
        /* --- USER_DETAILS TABLE --- */
        String sqlDetails = "insert into user_details (username, firstname, lastname, phonenumber) values (?, ?, ?, ?)";
        conn = null;
        
        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sqlDetails);
            ps.setString(1, user.getUsername());
            ps.setString(2, user.getFirstname());
            ps.setString(3, user.getLastname());
            ps.setString(4, user.getPhonenumber());
            ps.executeUpdate();
            ps.close();
            
        } catch (SQLException e) {
            throw new RuntimeException(e);
            
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {}
            }
        }
        
        /* --- USER_ROLES TABLE --- */
        String sqlRoles  = "insert into user_roles (username, role) values (?, ?)";
        conn = null;
        
        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sqlRoles);
            ps.setString(1, user.getUsername());
            ps.setString(2, "ROLE_USER");
            ps.executeUpdate();
            ps.close();
            
        } catch (SQLException e) {
            throw new RuntimeException(e);
            
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {}
            }
        }
    }
}
