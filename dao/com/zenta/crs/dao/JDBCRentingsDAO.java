package com.zenta.crs.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import javax.sql.DataSource;

import com.zenta.crs.model.Rent;

public class JDBCRentingsDAO implements RentingsDAO {

    private DataSource dataSource;
    
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    
    @Override
    public void createRent(Rent rent) {
        String sql = "insert into rentings (username, license_id, rent_from, rent_to, pickup_location, dropdown_location, total_price) values (?, ?, ?, ?, ?, ?, ?)";
        Connection conn = null;
        
        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, rent.getUsername());
            ps.setString(2, rent.getLicense_id());
            ps.setDate(3, rent.getRent_from());
            ps.setDate(4, rent.getRent_to());
            ps.setInt(5, rent.getPickup_location());
            ps.setInt(6, rent.getDropdown_location());
            ps.setInt(7, rent.getTotalPrice());
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
            
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {}
            }
        }
    }

    @Override
    public void deleteRent(String rentId) {
        StringBuilder sb = new StringBuilder();
        sb.append("delete from rentings where renting_id='");
        sb.append(rentId);
        sb.append("'");
        String sql = sb.toString();
        Connection conn = null;
        
        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
            
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {}
            }
        }
    }
    
    @Override
    public Rent getNextRent(String licenseId, Date to) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from rentings where license_id = '");
        sb.append(licenseId); //PDF-571'
        sb.append("' and rent_from > '");
        sb.append(to); //2019-11-29'
        sb.append("' order by rent_to asc limit 1");
        System.out.println(sb.toString());
        System.out.println(readDB(sb.toString()).size());
        if (readDB(sb.toString()).size() == 0) {
            return null;
        } else {
            return readDB(sb.toString()).get(0);
        }
    }

    @Override
	public Rent getOverlappingRents(String licenseId, Date from, Date to) {
    	StringBuilder sb = new StringBuilder();
    	sb.append("select * from rentings where license_id = '");
    	sb.append(licenseId); //KJS-123
    	sb.append("' and (rent_from >= '");
    	sb.append(from); //2019-12-04
    	sb.append("' or rent_to >= '");
    	sb.append(from); //2019-12-04
    	sb.append("') and (rent_to <= '");
    	sb.append(to); //2019-12-09
    	sb.append("' or rent_from <= '");
    	sb.append(to); //2019-12-09
    	sb.append("') order by rent_from desc");
        if (readDB(sb.toString()).size() == 0) {
            return null;
        } else {
            return readDB(sb.toString()).get(0);
        }
	}

	@Override
	public Rent getOverlappingRentsWithExclude(String licenseId, Date from, Date to, int rentId) {
    	StringBuilder sb = new StringBuilder();
    	sb.append("select * from rentings where license_id = '");
    	sb.append(licenseId); //KJS-123
    	sb.append("' and (rent_from >= '");
    	sb.append(from); //2019-12-04
    	sb.append("' or rent_to >= '");
    	sb.append(from); //2019-12-04
    	sb.append("') and (rent_to <= '");
    	sb.append(to); //2019-12-09
    	sb.append("' or rent_from <= '");
    	sb.append(to); //2019-12-09
    	sb.append("') and renting_id != ");
    	sb.append(rentId);
    	sb.append(" order by rent_from desc");
        if (readDB(sb.toString()).size() == 0) {
            return null;
        } else {
            return readDB(sb.toString()).get(0);
        }
	}

	@Override
    public Rent getPreviousRent(String licenseId, Date from) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from rentings where license_id = '");
        sb.append(licenseId); //PDF-571'
        sb.append("' and rent_to < '");
        sb.append(from); //2019-11-29'
        sb.append("' order by rent_from desc limit 1");
        System.out.println(sb.toString());
        System.out.println(readDB(sb.toString()).size());
        if (readDB(sb.toString()).size() == 0) {
            return null;
        } else {
            return readDB(sb.toString()).get(0);
        }
    }

    @Override
    public Rent getRent(String rentingId) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from rentings where renting_id='");
        sb.append(rentingId);
        sb.append("'");
        String sql = sb.toString();
        return readDB(sql).get(0);
    }

	@Override
    public ArrayList<Rent> getRentsByDate(Date date) {

        StringBuilder sb = new StringBuilder();
        sb.append("select * from rentings where '");
        sb.append(date);
        sb.append("' between rent_from and rent_to");
        String sql = sb.toString();
        return readDB(sql);
    }

    @Override
    public ArrayList<Rent> getRentsByDates(Date from, Date to) {

        StringBuilder sb = new StringBuilder();
        sb.append("select * from rentings where '");
        sb.append(from);
        sb.append("' between rent_from and rent_to or '");
        sb.append(to);
        sb.append("' between rent_from and rent_to or rent_from between '");
        sb.append(from);
        sb.append("' and '");
        sb.append(to);
        sb.append("'");
        String sql = sb.toString();
        return readDB(sql);
    }

    @Override
    public ArrayList<Date> getReservedDatesByLicenceId(String licenseId) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from rentings where license_id='");
        sb.append(licenseId);
        sb.append("' and rent_to>curdate()");
        String sql = sb.toString();
        ArrayList<Rent> rents = readDB(sql);
        ArrayList<Date> reservedDates = new ArrayList<>();
        for(Rent rent : rents) {
            long rawInterval = rent.getRent_to().getTime() - rent.getRent_from().getTime();
            long interval = TimeUnit.DAYS.convert(rawInterval, TimeUnit.MILLISECONDS);
            for(int i = 0; i-1 < interval; ++i) {
                Calendar c = Calendar.getInstance(); 
                c.setTime(rent.getRent_from()); 
                c.add(Calendar.DATE, i);
                Date nextDate = new Date(c.getTimeInMillis());
                reservedDates.add(nextDate);
            }
        }
        return reservedDates;
    }

    @Override
    public ArrayList<Date> getReservedDatesByLicenceId(String licenseId, String rentingId) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from rentings where license_id='");
        sb.append(licenseId);
        sb.append("' and rent_to>curdate() and renting_id!='");
        sb.append(rentingId);
        sb.append("'");
        String sql = sb.toString();
        ArrayList<Rent> rents = readDB(sql);
        ArrayList<Date> reservedDates = new ArrayList<>();
        for(Rent rent : rents) {
            long rawInterval = rent.getRent_to().getTime() - rent.getRent_from().getTime();
            long interval = TimeUnit.DAYS.convert(rawInterval, TimeUnit.MILLISECONDS);
            for(int i = 0; i-1 < interval; ++i) {
                Calendar c = Calendar.getInstance(); 
                c.setTime(rent.getRent_from()); 
                c.add(Calendar.DATE, i);
                Date nextDate = new Date(c.getTimeInMillis());
                reservedDates.add(nextDate);
            }
        }
        return reservedDates;
    }
    
    @Override
	public boolean isRestricted(String rentingId, String email) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from rentings where renting_id='");
        sb.append(rentingId);
        sb.append("' and username = '");
        sb.append(email);
        sb.append("'");
        System.out.println("EZ A LEKÉRDEZÉS: " + sb.toString());
        System.out.println("HA IGAZ AKKOR 0: " + readDB(sb.toString()).size());
        if (readDB(sb.toString()).size() == 0) {
            return true;
        } else {
        	return false;
        }
	}

    @Override
	public ArrayList<Rent> listActiveRents() {
		String sql = "select * from rentings where rent_from >= curdate() order by rent_from asc";
		return readDB(sql);
	}
    
    @Override
    public ArrayList<Rent> listRents(String email) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from rentings where username='");
        sb.append(email);
        sb.append("' order by rent_from desc");
        String sql = sb.toString();
        return readDB(sql);
    }

	@Override
    public int modifyRent(Rent rent) {
        StringBuilder sb = new StringBuilder();
        sb.append("update rentings set rent_from='");
        sb.append(rent.getRent_from());
        sb.append("', rent_to='");
        sb.append(rent.getRent_to());
        sb.append("', pickup_location='");
        sb.append(rent.getPickup_location());
        sb.append("', dropdown_location='");
        sb.append(rent.getDropdown_location());
        sb.append("', total_price='");
        sb.append(rent.getTotalPrice());
        sb.append("' where renting_id='");
        sb.append(rent.getRenting_id());
        sb.append("'");
        String sql = sb.toString();
        Connection conn = null;
        
        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
            
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {}
            }
        }
        return 0;
    }    
    
    public ArrayList<Rent> readDB(String sql) {
        ArrayList<Rent> rents = new ArrayList<>();
        Connection conn = null;
        
        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            Rent rent = null;
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                rent = new Rent(
                    rs.getInt("renting_id"),
                    rs.getString("username"), 
                    rs.getString("license_id"), 
                    rs.getDate("rent_from"), 
                    rs.getDate("rent_to"), 
                    rs.getInt("pickup_location"),
                    rs.getInt("dropdown_location"),
                    rs.getInt("total_price")
                );
                rents.add(rent);
            }
            rs.close();
            ps.close();
            return rents;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                try {
                conn.close();
                } catch (SQLException e) {}
            }
        }
    }
}
