package com.zenta.crs.dao;

import java.sql.Date;
import java.util.ArrayList;

import com.zenta.crs.model.Rent;

public interface RentingsDAO {
    public void createRent(Rent rent);
    public void deleteRent(String rentId);
    public Rent getNextRent(String licenseId, Date to);
    public Rent getOverlappingRents(String licenseId, Date from, Date to);
    public Rent getOverlappingRentsWithExclude(String licenseId, Date from, Date to, int rentId);
    public Rent getPreviousRent(String licenseId, Date from);
    public Rent getRent(String rentingId);
    public ArrayList<Rent> getRentsByDate(Date date);
    public ArrayList<Rent> getRentsByDates(Date from, Date to);
    public ArrayList<Date> getReservedDatesByLicenceId(String licenseId);
    public ArrayList<Date> getReservedDatesByLicenceId(String licenseId, String rentingId);
    public boolean isRestricted(String rentingId, String email);
    public ArrayList<Rent> listRents(String email);
    public ArrayList<Rent> listActiveRents();
    public int modifyRent(Rent rent);
}
