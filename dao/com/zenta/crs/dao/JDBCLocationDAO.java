package com.zenta.crs.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.sql.DataSource;
import com.zenta.crs.dao.LocationDAO;
import com.zenta.crs.model.Location;

public class JDBCLocationDAO implements LocationDAO {

    private DataSource dataSource;
    
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    
    @Override
    public Location getLocationById(int locationId) {
        String sql = "SELECT * FROM LOCATION WHERE LOCATION_ID=?";
        
        Connection conn = null;
        
        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, locationId);
            Location location = null;
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                location = new Location(
                    rs.getInt("LOCATION_ID"),
                    rs.getString("NAME"), 
                    rs.getString("ADDRESS"), 
                    rs.getString("GPS"), 
                    rs.getInt("STALLS"),
                    rs.getString("PHONE"), 
                    rs.getString("EMAIL")
                );
            }
            rs.close();
            ps.close();
            return location;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                try {
                conn.close();
                } catch (SQLException e) {}
            }
        }
    }
    
    
    
    @Override
	public int getStalls(int locationId) {
    	StringBuilder sb = new StringBuilder();
    	sb.append("select stalls from location where location_id='");
        sb.append(locationId);
        sb.append("'");
        
        Connection conn = null;
        
        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sb.toString());
            ResultSet rs = ps.executeQuery();
            int stalls = 0;
            if (rs.next()) {
                stalls = rs.getInt("stalls");
            }
            rs.close();
            ps.close();
            return stalls;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                try {
                conn.close();
                } catch (SQLException e) {}
            }
        }
    }

	@Override
    public ArrayList<Location> listLocations() {
        String sql = "SELECT * FROM LOCATION";
        
        ArrayList<Location> locations = new ArrayList<Location>();
        Connection conn = null;
        
        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            Location location = null;
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                location = new Location(
                    rs.getInt("LOCATION_ID"),
                    rs.getString("NAME"), 
                    rs.getString("ADDRESS"), 
                    rs.getString("GPS"), 
                    rs.getInt("STALLS"),
                    rs.getString("PHONE"), 
                    rs.getString("EMAIL")
                );
                locations.add(location);
            }
            /*System.out.println(location);*/
            rs.close();
            ps.close();
            return locations;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                try {
                conn.close();
                } catch (SQLException e) {}
            }
        }
    }

}
