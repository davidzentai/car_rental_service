package com.zenta.crs.dao;

import java.util.ArrayList;
import com.zenta.crs.model.Location;

public interface LocationDAO {
    public Location getLocationById(int locationId);
    public int getStalls(int locationId);
    public ArrayList<Location> listLocations();
}
