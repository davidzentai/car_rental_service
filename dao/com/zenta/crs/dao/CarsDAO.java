package com.zenta.crs.dao;

import java.util.ArrayList;

import com.zenta.crs.model.Car;

public interface CarsDAO {
    public void createCar(Car car);
    public void deleteCar(String licenseId);
    public Car getCarByLicenseId(String license_id);
    public int getCarPriceByLicenseId(String licenseId);
    public ArrayList<Car> getCarsFromDB(String sql);
    public ArrayList<String> getCategories();
    public ArrayList<String> getLicenseIds();
    public ArrayList<Car> listAll();
    public void modifyCar(Car car);
    public ArrayList<Car> searchByParams(ArrayList<String> categories);
    public void writeDB(String sql);
}
