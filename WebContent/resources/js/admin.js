var available = document.getElementsByName('available');
for(var i = 0; i < available.length; i++) {
	if(available[i].value == "true") {
		available[i].checked = true;
		available[i].value = true;
	} else {
		available[i].checked = false;
		available[i].value = false;
	}
}

function changeButtonColor(row) {
	var saveButtons = document.getElementsByName('save');
	saveButtons[row-1].style.filter = "hue-rotate(90deg)";
}

function uploadImage(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            document.getElementById("preview").src = e.target.result;
            document.getElementById("imageFile").value = e.target.result;
            /*document.getElementById("download").href =  e.target.result;
            document.getElementById("download").click();*/
        };
        reader.readAsDataURL(input.files[0]);
    }
}


function validateNewCarForm() {
	var make = document.forms["newCarForm"]["make"];
    var model = document.forms["newCarForm"]["model"];
    var year =  document.forms["newCarForm"]["year"];
    var seats = document.forms["newCarForm"]["seats"];
    var doors = document.forms["newCarForm"]["doors"];
    var pricePerDay =  document.forms["newCarForm"]["pricePerDay"];
    var category = document.forms["newCarForm"]["category"];
    var image = document.forms["newCarForm"]["image"];
    var license_id = document.forms["newCarForm"]["license_id"];

    if (make.value == ""){ 
    	errorMessage("A márka nincs kitöltve.");
    	make.focus(); 
        return false; 
    } 

    if (model.value == ""){ 
    	errorMessage("A modell nincs kitöltve.");
    	model.focus(); 
        return false; 
    } 
    
    if (year.value == ""){ 
    	errorMessage("Az évjárat dátum nincs kitöltve.");
    	year.focus(); 
        return false; 
    } 
    
    if (seats.value == ""){ 
    	errorMessage("A ülések száma nincs kitöltve.");
    	seats.focus(); 
        return false; 
    }
   
    if (doors.value == ""){ 
    	errorMessage("Az ajtók száma nincs kitöltve.");
    	doors.focus(); 
        return false; 
    }

    if (pricePerDay.value == ""){ 
    	errorMessage("Az ár nincs kitöltve.");
    	pricePerDay.focus(); 
        return false; 
    } 
    
    if (category.value == ""){ 
    	errorMessage("A kategória nincs kiválasztva.");
    	category.focus(); 
        return false; 
    } 
    
    if (image.value == ""){ 
    	errorMessage("Nincs kép kiválasztva.");
    	image.focus(); 
        return false; 
    }
   
    if (license_id.value == ""){ 
    	errorMessage("A rendszám nincs kitöltve.");
    	license_id.focus(); 
        return false; 
    }

    if (make.value.indexOf("'", 0) >= 0){ 
    	errorMessage("A márka mezőben tiltott karakter.");
    	make.focus(); 
        return false; 
    } 

    if (model.value.indexOf("'", 0) >= 0){ 
    	errorMessage("A modell mezőben tiltott karakter.");
    	model.focus(); 
        return false; 
    } 
    
    if (year.value.indexOf("'", 0) >= 0){ 
    	errorMessage("Az évjárat dátum mezőben tiltott karakter.");
    	year.focus(); 
        return false; 
    } 
    
    if (seats.value.indexOf("'", 0) >= 0){ 
    	errorMessage("A ülések száma mezőben tiltott karakter.");
    	seats.focus(); 
        return false; 
    }
   
    if (doors.value.indexOf("'", 0) >= 0){ 
    	errorMessage("Az ajtók száma mezőben tiltott karakter.");
    	doors.focus(); 
        return false; 
    }

    if (pricePerDay.value.indexOf("'", 0) >= 0){ 
    	errorMessage("Az ár mezőben tiltott karakter.");
    	pricePerDay.focus(); 
        return false; 
    } 
    
    if (category.value.indexOf("'", 0) >= 0){ 
    	errorMessage("A kategória mezőben tiltott karakter.");
    	category.focus(); 
        return false; 
    } 
    
    if (image.value.indexOf("'", 0) >= 0){ 
    	errorMessage("Kép mezőben tiltott karakter.");
    	image.focus(); 
        return false; 
    }
   
    if (license_id.value.indexOf("'", 0) >= 0){ 
    	errorMessage("A rendszám mezőben tiltott karakter.");
    	license_id.focus(); 
        return false; 
    }
}

function errorMessage(message){
	document.getElementById("error").style.display = "block";
	document.getElementById("errorMessage").innerHTML = message;
}