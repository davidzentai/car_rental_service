function validateRegistrationForm(){
	var email = document.forms["registrationForm"]["email"];
    var lastname = document.forms["registrationForm"]["lastname"];
    var firstname =  document.forms["registrationForm"]["firstname"];
    var phonenumber = document.forms["registrationForm"]["phonenumber"];
    var password = document.forms["registrationForm"]["password"];
    var repassword = document.forms["registrationForm"]["repassword"];
       
    if (email.value == ""){
    	errorMessage("A megadott e-mail cím nem helyes.");
        email.focus(); 
        return false; 
    }
   
    if (email.value.indexOf("@", 0) < 0){
    	errorMessage("A megadott e-mail cím nem helyes.");
        email.focus();
        return false;
    } 
   
    if (email.value.indexOf(".", 0) < 0){ 
    	errorMessage("A megadott e-mail cím nem helyes.");
        email.focus(); 
        return false; 
    }

    if (lastname.value == ""){ 
    	errorMessage("A vezetéknév nincs kitöltve.");
        lastname.focus(); 
        return false; 
    } 
    
    if (firstname.value == ""){ 
    	errorMessage("A keresztnév nincs kitöltve.");
        firstname.focus(); 
        return false; 
    }
    
    if (phonenumber.value == ""){ 
    	errorMessage("A megadott telefonszám helytelen.");
        phonenumber.focus(); 
        return false; 
    } 
   
    if (password.value == ""){ 
    	errorMessage("Nem adtál meg jelszót.");
        password.focus(); 
        return false; 
    }
    
    if (repassword.value == ""){ 
    	errorMessage("Ismételd meg a jelszavad!");
        repassword.focus(); 
        return false; 
    }    
    
    if (password.value != repassword.value){
    	errorMessage("A megadott jelszavak nem egyeznek.");
        password.focus(); 
        return false; 
    }
    return true; 
}

function errorMessage(message){
	document.getElementById("error").style.display = "block";
	document.getElementById("errorMessage").innerHTML = message;
}
