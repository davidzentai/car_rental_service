if(new URL(document.URL).toString().includes("car?")) {
	if(document.getElementById("rentFrom").value == ''){
		getNextFreeDay("rentFrom");
	}
	if(document.getElementById("rentTo").value == ''){
		getNextFreeDay("rentTo");
	}
	setDisabledDates();
	getPrice();
}
else if(new URL(document.URL).toString().includes("index")) {
	/*if(document.getElementById("rentFrom").value == ''){
		getCurrentDate("rentFrom");
	}*/
	setDatepicker();
}

window.onload = checkInCategory();

function addParamToURL(param) {
	var url = new URL(document.URL);
	url.searchParams.delete("startDate");
	url.searchParams.append("startDate", param);
	window.location.replace(url);
	alert("url: " + url)
}

function checkCategories() {
	var url = new URL(document.URL);
	url.searchParams.delete("category");
	for (var i = 0; i < categories.length; i++){
		if (document.forms["searchbox"][categories[i]].checked == true) { 
			url.searchParams.append("category", categories[i]);
		}
	}
	window.location.replace(url);
}

function checkDate() {
	var rentFrom = document.getElementById("rentFrom").value;
	var rentTo = document.getElementById("rentTo").value;
	var today = setTodayDate();
	console.log(today + "<=" + rentFrom + "<=" + rentTo)
	if(rentFrom < today){
		errorMessage("A foglalás kezdetének időpontja nem lehet korábban a mai dátumnál.");
		if(new URL(document.URL).toString().includes("car?")) { getNextFreeDay("rentFrom"); }
	}
	else if(rentTo < today && rentTo != ''){
		errorMessage("A foglalási végének időpontja nem lehet korábban a mai dátumnál.");
		if(new URL(document.URL).toString().includes("car?")) { getNextFreeDay("rentTo"); }
	}
	else if(new URL(document.URL).toString().includes("car?")){
		getPrice();
	}
}

function checkInCategory() {
	var categories = document.getElementsByName("categoryElement");
	var elements = document.getElementsByClassName("card-deck");
	var ret = [];
	
	if(!isNoneCategoryCheckedIn(categories, elements)){
		categories.forEach(function (category, index) {
			if(category.checked) {
				ret.push("checked");
				for (var i = 0; i < elements.length; i++) {
					if(elements[i].title == category.id) {
						elements[i].style.display = "block";	
					} 
				}
			} else {
				ret.push("unchecked");
				for (var i = 0; i < elements.length; i++) {
					if(elements[i].title == category.id) {
						elements[i].style.display = "none";
					}
				}
			}
		});
	}
	
	document.getElementById("categories").value = ret;
}

function errorMessage(message){
	document.getElementById("error").style.display = "block";
	document.getElementById("errorMessage").innerHTML = message;
}

function getNextFreeDay(variable) {
	var nextFreeDay = setTodayDate();
	var array = document.getElementById("reservedDates").value;
	while (array.includes(nextFreeDay)) {
		var nextFreeDay = new Date(nextFreeDay.substring(0, 4), nextFreeDay.substring(5, 7) -1, nextFreeDay.substring(8, 10));
		nextFreeDay.setDate(nextFreeDay.getDate() + 1);
		var dd = String(nextFreeDay.getDate()).padStart(2, '0');
		var mm = String(nextFreeDay.getMonth() + 1).padStart(2, '0');
		var yyyy = nextFreeDay.getFullYear();
		nextFreeDay = yyyy + '-' + mm + '-' + dd;
	}
	var datepicker = document.getElementById(variable);
	datepicker.value = nextFreeDay;
}

function getCurrentDate(variable) {
	var tmp = document.getElementById(variable);
	tmp.value = setTodayDate();
}

function getPrice() {
	var one_day = 1000 * 60 * 60 * 24 ;
	var rentFrom = new Date(document.getElementById("rentFrom").value);
	var rentTo = new Date(document.getElementById("rentTo").value);
	var result = Math.round(rentTo.getTime() - rentFrom.getTime()) / (one_day);
	var days = parseInt(result.toFixed(0)) + 1;
	var oneDayPrice = parseInt(document.getElementById("oneDayPrice").value);
	var price = oneDayPrice * days;
	if (price > 0) {
		document.getElementById("totalPrice").innerHTML = price + " HUF";
	} else {
		document.getElementById("totalPrice").innerHTML = 0 + " HUF";
	}
}

function isNoneCategoryCheckedIn(categories, elements) {	
	var len = categories.length;
	
	categories.forEach(function (category, index) {
		if(category.checked) {
			len -= 1;
		}
	});
	if(len == categories.length){
		categories.forEach(function (category, index) {
			for (var i = 0; i < elements.length; i++) {
				if(elements[i].title == category.id) {
					elements[i].style.display = "block";
				}
			}
		});
		return true;
	} else {
		return false;
	}
}

function setDatepicker() {
	var today = setTodayDate();
	
	$("#rentFrom").datepicker({
		dateFormat: "yy-mm-dd",
		minDate: today
	});
	
	$("#rentTo").datepicker({
		dateFormat: "yy-mm-dd",
		minDate: today	
	});
}

function setDisabledDates() {	
	var today = setTodayDate();
	var array = document.getElementById("reservedDates").value;

	$("#rentFrom").datepicker({
		dateFormat: "yy-mm-dd",
		minDate: today,
	    beforeShowDay: function(date){	
	        var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
	        return [ array.indexOf(string) == -1 ]
	    }
	});
	
	$("#rentTo").datepicker({
		dateFormat: "yy-mm-dd",
		minDate: today,
	    beforeShowDay: function(date){	
	        var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
	        return [ array.indexOf(string) == -1 ]
	    }
	});
}

function setTodayDate() {
	var today = new Date();
	var dd = String(today.getDate()).padStart(2, '0');
	var mm = String(today.getMonth() + 1).padStart(2, '0');
	var yyyy = today.getFullYear();
	today = yyyy + '-' + mm + '-' + dd;
	return today;
}

function validateRentingForm() {
	var rentFrom = document.forms["rentingForm"]["rentFrom"];
    var rentTo = document.forms["rentingForm"]["rentTo"];
    var pickupLocation =  document.forms["rentingForm"]["pickupLocation"];
    var dropdownLocation = document.forms["rentingForm"]["dropdownLocation"];

    if (rentFrom.value == ""){ 
    	errorMessage("A kezdődátum nincs kitöltve.");
    	rentFrom.focus(); 
        return false; 
    } 
    
    if (rentTo.value == ""){ 
    	errorMessage("A leadási dátum nincs kitöltve.");
    	rentTo.focus(); 
        return false; 
    } 
    
    if (pickupLocation.value == ""){ 
    	errorMessage("A felvételi hely nincs kiválasztva.");
    	pickupLocation.focus(); 
        return false; 
    }
   
    if (dropdownLocation.value == ""){ 
    	errorMessage("A leadási hely nincs kiválasztva.");
    	dropdownLocation.focus(); 
        return false; 
    }
}