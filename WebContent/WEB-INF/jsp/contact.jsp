<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="/resources/style/style.css">
<title>Contacts</title>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
</head>
<body>
		<%@include file="/WEB-INF/layout/header.jsp"%>
		<div id="container">
			<div id="content">
				<div class="card-deck">
					<c:forEach items="${requestScope.locations}" var="location">
						<div class="card card-color mb-3" style="max-width: 400px">
						  <img src="/resources/img/<c:out value="${location.name}"></c:out>.jpg" class="card-img-top" alt="...">
						  <div class="card-color card-body">
						    <h5 class="card-color card-title"><c:out value="${location.name}"></c:out></h5>
						    <p class="card-color card-text"><c:out value="${location.address}"></c:out></p>
						  </div>
						  <ul class="card-color list-group list-group-flush">
						    <li class="card-color list-group-item">GPS: <c:out value="${location.GPS}"></c:out></li>
						    <li class="card-color list-group-item">Nyitvatartás:
						    	<br>H-P: 8:00 - 20:00
						    	<br>SZ-V:	08:00 - 16:00
						    </li>
						    <li class="card-color list-group-item">Telefon: <c:out value="${location.phone}"></c:out></li>
						    <li class="card-color list-group-item">E-mail: <c:out value="${location.email}"></c:out></li>
						  </ul>
					  </div>
				  </c:forEach>
			  </div>
			  <br>
			</div>
			<%@include file="/WEB-INF/layout/footer.jsp"%>
		</div>
</body>
</html>