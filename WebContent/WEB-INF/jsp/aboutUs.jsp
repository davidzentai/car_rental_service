<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<title>Rólunk</title>
</head>
<body>
	<%@include file="/WEB-INF/layout/header.jsp"%>
	<div id="container">
		<div id="content">
	       	<h1>Kedves látogató.</h1>
	       	<br>
	       	<p>Köszönjük, hogy felkereste oldalunkat. Nézzen körbe és kölcsönözzön nálunk!</p>
	       	<br>
	       	<h3>Beszt Top Námbör ván autókölcsönző cég vagyunk Magyarországon.</h3>
		</div>
		<%@include file="/WEB-INF/layout/footer.jsp"%>
	</div>
</body>
</html>