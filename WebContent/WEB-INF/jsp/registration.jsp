<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="/resources/style/style.css">
	<title>Registration</title>
</head>
<body>
	<%@include file="/WEB-INF/layout/header.jsp"%>
	<div id="container">
		<div id="content">
			<table class="content">
				<tr>
					<td id="edge">
						<div>
							<h1>Mi�rt �ri meg regisztr�lni?</h1>
							<ul>
								<li>Jelenleg regisztr�ci� n�lk�l nem lehets�ges az aut�b�rl�s.</li>
								<li>Egy�b hangzatos marketingsz�vegek...</li>
							</ul>
						</div>
					</td>
					<td id="center">
						<div id="verticalLine"></div>
					</td>
					<td id="edge" align="center">
						<div>
							<h2>Regisztr�ci�</h2>
							<!--  ERROR FROM BACK-END -->
							<c:if test="${not empty error}">
								<div id="error">
									<p id="errorMessage">${error}
								</div>
							</c:if>
							<!--  ERROR FROM FRONT-END -->
						 	<div id="error" style="display:none">
						 			<p id="errorMessage">
						 	</div>
				
							<form:form name='registrationForm' action="registrationSuccess" onsubmit="return validateRegistrationForm()" method='POST'>
							  <table class="form">
									<tr>
										<td>E-mail: </td>
										<td><input type='email' name='email' class="form-control-plaintext form" value='' autofocus="autofocus"></td>
									</tr>
									<tr>
										<td>Vezet�kn�v: </td>
										<td><input type='text' name='lastname' class="form-control-plaintext form" value=''></td>
									</tr>
									<tr>
										<td>Keresztn�v: </td>
										<td><input type='text' name='firstname' class="form-control-plaintext form" value=''></td>
									</tr>
									<tr>
										<td>Telefon: </td>
										<td><input type='text' name='phonenumber' class="form-control-plaintext form" value=''></td>
									</tr>
									<tr>
										<td>Jelsz�: </td>
										<td><input type='password' name='password' class="form-control-plaintext form"></td>
									</tr>
									<tr>
										<td>Jelsz� �jra: </td>
										<td><input type='password' name='repassword' class="form-control-plaintext form"></td>
									</tr>
									<tr>
										<td colspan='2' style="text-align: center"><input name="submit" class="btn btn-color" type="submit" value="Regisztr�ci�"/></td>
									</tr>
							  </table>
					
							  <input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
					
							</form:form>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<%@include file="/WEB-INF/layout/footer.jsp"%>
	</div>
</body>
<script src="/resources/js/checkForms.js"></script>
</html>
