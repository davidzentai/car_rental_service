<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page session="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="/resources/style/style.css">
	<title>Welcome</title>
</head>
<body>
	<%@include file="/WEB-INF/layout/header.jsp"%>
	<div id="container">
		<div id="content">
		<!--  MESSAGE FROM BACK-END -->
		<c:choose>
			<c:when test="${not empty msg}">
				<div class="col" id="msg">${msg}</div>
			</c:when>
			<c:otherwise>
				<div class="col" id="error">
					<p id="errorMessage">Kérlek mindig gondoskodj róla, ha törölsz egy foglalást az nem lehetetlenít e el rákövetkező foglalást! Ha igen, gondoskodj a másik telephelyre való szállításról!
				</div>
			</c:otherwise>
		</c:choose>
		<c:if test="${not empty error}">
			<div class="col" id="error">
				<p id="errorMessage">${error}
			</div>
		</c:if>
		<table class="table">
		  <thead>
		    <tr class="main-fontcolor">
		      <th scope="col">#</th>
		      <th scope="col">E-mail</th>
		      <th scope="col">Rendszám</th>
		      <th scope="col">Azonosító</th>
		      <th scope="col">Foglalás kezdete</th>
		      <th scope="col">Foglalás vége</th>
		      <th scope="col">Felvétel helye</th>
		      <th scope="col">Leadás helye</th>
		      <th scope="col"></th>
		    </tr>
		  </thead>
		  <tbody>
		  	<c:set value="${requestScope.pickupLocations}" var="pickupLocations"></c:set>
		  	<c:set value="${requestScope.dropdownLocation}" var="dropdownLocation"></c:set>
		  	<c:set var="rownumber" value="1" scope="page"></c:set>
		  	<c:forEach items="${requestScope.rents}" var="rent">
			    <tr>
			      <th><c:out value="${rownumber}"></c:out></th>
			      <td><c:out value="${rent.username}"></c:out></td>
			      <td><c:out value="${rent.license_id}"></c:out></td>
			      <td><c:out value="${rent.renting_id}"></c:out></td>
			      <td><c:out value="${rent.rent_from}"></c:out>
			      <td><c:out value="${rent.rent_to}"></c:out></td>
			      <td><c:out value="${pickupLocations[rownumber-1]}"></c:out></td>
			      <td><c:out value="${dropdownLocation[rownumber-1]}"></c:out></td>
			      <form:form action="deleteRentAdmin">
		      		<td><button class="list-button" id="delete" name="delete" type="submit" value="<c:out value="${rent.renting_id}"></c:out>"><img alt="delete" src="/resources/img/delete-icon.png"></button></td>
			      </form:form>
			    </tr>
		  		<c:set var="rownumber" value="${rownumber + 1}" scope="page"></c:set>
		    </c:forEach>
		  </tbody>
		</table>		
		</div>
	<%@include file="/WEB-INF/layout/footer.jsp"%>
	</div>
</body>
</html>