<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="true"%>
<%@ page import="java.util.Date" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="/resources/style/style.css">
	<title>Autó felvétele</title>
</head>
<body>
	<%@include file="/WEB-INF/layout/header.jsp"%>
	<div id="container">
		<div id="content">
    		<form:form action="createCar" name="newCarForm" onsubmit="return validateNewCarForm()">
				<!--  MESSAGE FROM BACK-END -->		
				<c:if test="${not empty msg}">
					<div class="col" id="msg">${msg}</div>
				</c:if>
				<!--  ERROR FROM FRONT-END -->
			 	<div class="col" id="error" style="display:none">
		 			<p id="errorMessage">
				</div>
				
				<div class="row">
			  		<div class="col-6"><h1 class="main-fontcolor">Autó adatai:</h1></div>
				</div>
				<div class="row">
					<div class="col empty-row"></div>
				</div>
    			<div class="row">
    				<div class="col-6">
						<div class="row row-padding">
							<div class="col-4">
								<div class="short-div h5-font">Autó márkája: </div>
							</div>
							<div class="col-4">
								<div class="short-div h5-font"><input class="list-input" id="make" name="make" type="text"></div>
							</div>
						</div>
						<div class="row row-padding">
							<div class="col-4">
								<div class="short-div h5-font">Modell: </div>
							</div>
							<div class="col-4">
								<div class="short-div h5-font"><input class="list-input" id="model" name="model" type="text"></div>
							</div>
						</div>
						<div class="row row-padding">
							<div class="col-4">
								<div class="short-div h5-font">Évjárat: </div>
							</div>
							<div class="col-4">
								<div class="short-div h5-font"><input class="list-input" id="year" name="year" type="number"></div>
							</div>
						</div>
						<div class="row row-padding">
							<div class="col-4">
								<div class="short-div h5-font">Ülések száma: </div>
							</div>
							<div class="col-4">
								<div class="short-div h5-font"><input class="list-input" id="seats" name="seats" type="number"></div>
							</div>
						</div>
						<div class="row row-padding">
							<div class="col-4">
								<div class="short-div h5-font">Ajtók száma: </div>
							</div>
							<div class="col-4">
								<div class="short-div h5-font"><input class="list-input" id="doors" name="doors" type="number"></div>
							</div>
						</div>
						<div class="row row-padding">
							<div class="col-4">
								<div class="short-div h5-font">Manuális váltó: </div>
							</div>
							<div class="col-4">
								<div class="short-div h5-font"><input class="list-checkbox checkbox-margin" id="manual_gearbox" name="manual_gearbox" type="checkbox"></div>
							</div>
						</div>
						<div class="row row-padding">
							<div class="col-4">
								<div class="short-div h5-font">Aktív státusz: </div>
							</div>
							<div class="col-4">
								<div class="short-div h5-font"><input class="list-checkbox checkbox-margin" id="available" name="available" type="checkbox"></div>
							</div>
						</div>
						<div class="row row-padding">
							<div class="col-4">
								<div class="short-div h5-font">Napi ár: </div>
							</div>
							<div class="col-4">
								<div class="short-div h5-font"><input class="list-input" id="pricePerDay" name="pricePerDay" type="number"></div>
							</div>
						</div>
						<div class="row row-padding">
							<div class="col-4">
								<div class="short-div h5-font">Kategória: </div>
							</div>
					    <div class="col-4">
						    <select id="category" name="category" class="form-control btn-color">
							  	<c:forEach items="${requestScope.categories}" var="category">
							    	<option class="btn-color" value="<c:out value="${category}"></c:out>"><c:out value="${category}"></c:out></option>
							    </c:forEach>
						   	</select>
					   	</div>
				   	</div>
						<div class="row row-padding">
							<div class="col-4">
								<div class="short-div h5-font">Profil kép: </div>
							</div>
							<div class="col-4">
								<div class="short-div"><input class="list-input" id="image" name="image" type="file" onchange="uploadImage(this)"></div>
								<!-- <div class="short-div h5-font"><input class="list-input" id="image" name="image" type="file"></div> -->
							</div>
				   	</div>
						<div class="row row-padding">
							<div class="col-4">
								<div class="short-div h5-font">Rendszám: </div>
							</div>
							<div class="col-4">
								<div class="short-div h5-font"><input class="list-input" id="license_id" name="license_id" type="text"></div>
							</div>
				   	</div>
						<div class="row row-padding">
							<div class="col-5">
								<!-- <button class="btn btn-color btn-maxlen" type="submit">Mentés</button> -->
								<input name="submit" class="btn btn-color btn-maxlen" type="submit" value="Mentés"/>
					   	</div>
				   	</div>
					</div>
					<div class="col-6">
						<div class="row row-padding">
							<div class="col-4">
								<div class="short-div h5-font">Kép előnézet: </div>
							</div>
						</div>
						<div class="row row-padding">						
							<div class="col">
								<!-- <a id="download" href="" style="display: none;" download></a> -->
								<img class="img-border img-preview" id="preview" src="https://via.placeholder.com/600x400/2A2A2A/CBFA2F?text=A+képméret+legyen+600x400!">
								<object style="display: none;">
									<input id="imageFile" name="imageFile" value="">
								</object>
							</div>
				   		</div>
					</div>
				</div>
			</form:form>	
		</div>
		<%@include file="/WEB-INF/layout/footer.jsp"%>
	</div>
	<script src="/resources/js/admin.js"></script>
</body>
</html>