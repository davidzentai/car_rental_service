<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="/resources/style/style.css">
<title>Login</title>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
</head>
<body>
	<%@include file="/WEB-INF/layout/header.jsp"%>
	<div id="container">
		<div id="content">
			<table class="content">
				<tr>
					<td id="edge">
						<div>
							<h1>Mi�rt �ri meg regisztr�lni?</h1>
							<ul>
								<li>Jelenleg regisztr�ci� n�lk�l nem lehets�ges az aut�b�rl�s.</li>
								<li>Egy�b hangzatos marketingsz�vegek...</li>
							</ul>
						</div>
					</td>
					<td id="center">
						<div id="verticalLine"></div>
					</td>
					<td id="edge" align="center">
						<div>
							<h2>Bejelentkez�s</h2>
							<c:if test="${not empty error}">
								<div id="error">
									<p id="errorMessage">${error}
								</div>
							</c:if>
							<c:if test="${not empty msg}">
								<div id="msg">${msg}</div>
							</c:if>
				
							<form name='loginForm' action="<c:url value='j_spring_security_check' />" method='POST'>
					
							  <table class="form">
									<tr>
										<td>E-mail:</td>
										<td><input type='text' name='username' value='' class="form-control-plaintext form" autofocus="autofocus"></td>
									</tr>
									<tr>
										<td>Jelsz�:</td>
										<td><input type='password' name='password' class="form-control-plaintext form"/></td>
									</tr>
									<tr>
										<td colspan='2' style="text-align: center"><input name="submit" class="btn btn-color" type="submit" value="Bejelentkez�s" /></td>
									</tr>
							  </table>
					
							  <input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
					
							</form>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<%@include file="/WEB-INF/layout/footer.jsp"%>
	</div>
</body>
</html>
