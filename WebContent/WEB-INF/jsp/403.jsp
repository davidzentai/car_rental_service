<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="/resources/style/style.css">
<title>Access Denied</title>
</head>
<body>
	<%@include file="/WEB-INF/layout/header.jsp"%>
	<div id="container">
		<div id="content">
			<div class="error-code">ERROR: 403</div>
			<div class="error-text">You do not have permission to access this page!</div>
		</div>
	<%@include file="/WEB-INF/layout/footer.jsp"%>
	</div>
</body>
</html>