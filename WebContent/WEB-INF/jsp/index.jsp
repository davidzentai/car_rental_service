<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="/resources/style/style.css">
	
	<!-- jQuery is required for the datepicker  -->
	<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/themes/redmond/jquery-ui.css">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
	<title>Car Rental Service</title>
</head>
<body>
		<%@include file="/WEB-INF/layout/header.jsp"%>
		<div id="container">
			<div id="content">
				<!-- SEARCHBOX -->
				<form:form name="searchbox" action="index">
					<div class="row">
						<!--  ERROR FROM BACK-END -->
						<c:if test="${not empty error}">
							<div id="error">
								<p id="errorMessage">${error}
							</div>
						</c:if>
						<!--  ERROR FROM FRONT-END -->
					 	<div class="col" id="error" style="display:none">
					 			<p id="errorMessage">
						</div>
					</div>
					<div class="row">
				    <div class="col"></div>
				    <div class="col">
					    <input onchange="checkInCategory()" type="checkbox" class="checkbox-input" id="saloon" name="categoryElement" <c:out value="${requestScope.categories0}"></c:out>/>
							<label class="category-selector" for="saloon"><img src="/resources/img/normal.png" /></label>
						</div>
				    <div class="col">
					    <input onchange="checkInCategory()" type="checkbox" class="checkbox-input" id="estate" name="categoryElement" <c:out value="${requestScope.categories1}"></c:out>/>
							<label class="category-selector" for="estate"><img src="/resources/img/kombi.png" /></label>
						</div>
				    <div class="col">
					    <input onchange="checkInCategory()" type="checkbox" class="checkbox-input" id="7plus" name="categoryElement" <c:out value="${requestScope.categories2}"></c:out>/>
							<label class="category-selector" for="7plus"><img src="/resources/img/7+.png" alt="7+"></label>
						</div>
				    <div class="col">
					    <input onchange="checkInCategory()" type="checkbox" class="checkbox-input" id="cabrio" name="categoryElement" <c:out value="${requestScope.categories3}"></c:out>/>
							<label class="category-selector" for="cabrio"><img src="/resources/img/cabrio.png" alt="Cabrio"></label>
						</div>
				    <div class="col">
					    <input onchange="checkInCategory()" type="checkbox" class="checkbox-input" id="coupe" name="categoryElement" <c:out value="${requestScope.categories4}"></c:out>/>
							<label class="category-selector" for="coupe"><img src="/resources/img/coupe.png" alt="Coupe"></label>
						</div>
				    <div class="col">
					    <input onchange="checkInCategory()" type="checkbox" class="checkbox-input" id="pickup" name="categoryElement" <c:out value="${requestScope.categories5}"></c:out>/>
							<label class="category-selector" for="pickup"><img src="/resources/img/offroad.png" alt="Off-road"></label>
						</div>
				    <div class="col">
					    <input onchange="checkInCategory()" type="checkbox" class="checkbox-input" id="SUV" name="categoryElement" <c:out value="${requestScope.categories6}"></c:out>/>
							<label class="category-selector" for="SUV"><img src="/resources/img/suv.png" alt="SUV"></label>
						</div>
				    <div class="col">
					    <input onchange="checkInCategory()" type="checkbox" class="checkbox-input" id="van" name="categoryElement" <c:out value="${requestScope.categories7}"></c:out>/>
							<label class="category-selector" for="van"><img src="/resources/img/teher.png" alt="Teherautó"></label>
						</div>
				    <div class="col"></div>
				  </div>
				  <div class="row">
				  	<div class="col empty-row"></div>
				  </div>
				  <div class="row">
				    <div class="col"></div>
				    <div class="col-2">Foglalás első napja: </div>
				    <div class="col-2"><input type='text' id='rentFrom' name='rentFrom' class="form-control-plaintext form" onchange="checkDate()" value="<c:out value="${requestScope.rentFrom}"></c:out>" readonly></div>
				    <div class="col"></div>
				    <div class="col-2">Foglalás utolsó napja: </div>
				    <div class="col-2"><input type='text' id='rentTo' name='rentTo' class="form-control-plaintext form" onchange="checkDate()" value="<c:out value="${requestScope.rentTo}"></c:out>" readonly></div>
				    <div class="col"></div>
					</div>
				  <div class="row">
				  	<div class="col empty-row"></div>
				  </div>
					<div class="row">
				    <div class="col"></div>
				    <div class="col-2">Kölcsönzési telephely: </div>
				    <div class="col-2">
					    <select id="fromLocation" name="fromLocation" class="form-control btn-color">
					    	<c:choose>
					    		<c:when test="${requestScope.fromLocation != null}">
							  		<option selected class="btn btn-color" value="<c:out value="${requestScope.fromLocation}"></c:out>"><c:out value="${locations[fromLocation].name}"></c:out></option>
					    		</c:when>
					    		<c:otherwise>
							  		<option selected class="btn btn-color" value='' >Felvétel helye</option>
					    		</c:otherwise>
					    	</c:choose>
						  	<c:forEach items="${requestScope.locations}" var="locations">
						    	<option class="btn-color" value="<c:out value="${locations.locationId}"></c:out>"><c:out value="${locations.name}"></c:out></option>
						    </c:forEach>
					    </select>
				    </div>
				    <div class="col"></div>
				    <div class="col-2">Leadási telephely: </div>
				    <div class="col-2">
					    <select id="toLocation" name="toLocation" class="form-control btn-color">
					    	<c:choose>
					    		<c:when test="${requestScope.fromLocation != null}">
							  		<option selected class="btn btn-color" value="<c:out value="${requestScope.toLocation}"></c:out>"><c:out value="${locations[toLocation].name}"></c:out></option>
					    		</c:when>
					    		<c:otherwise>
							  		<option selected class="btn btn-color" value=''>Leadás helye</option>
					    		</c:otherwise>
					    	</c:choose>
						  	<c:forEach items="${requestScope.locations}" var="locations">
						    	<option class="btn-color" value="<c:out value="${locations.locationId}"></c:out>"><c:out value="${locations.name}"></c:out></option>
						    </c:forEach>
					    </select>
						</div>
				    <div class="col"></div>
					</div>
					<div class="row">
				  	<div class="col empty-row"></div>
				  </div>
				  <div class="row">
				    <div class="col-5"></div>
				  	<div class="col">
				  		<object hidden="true">
				  			<input id="categories" name="categories" >
				  		</object>
				  		<input name="submit" class="btn btn-color" type="submit" value="Keresés" style="width: 100%"/>
				  	</div>
				    <div class="col-2"></div>
				  	<div class="col-3">
				  		<a class="btn btn-color" style="width: 100%" href="/index"/>Keresési feltételek törlése</a>
				  	</div>
				  </div>
				</form:form>
				<br>
				<!-- RESOULTS -->
				<c:forEach items="${requestScope.cars}" var="car">
				<c:choose>
					<c:when test="${car.available}">
						<div class="card-color card-deck card-space" id="card-category" title="<c:out value="${car.category}"></c:out>">
							<div class="card-color card"  style="bottom-margin: 10px;">
								<div class="card-color row">
									<div class="card-color col-9">
									  <div class="card-color card-body">
									    <h5 class="card-color card-title"><c:out value="${car.make}"></c:out> - <c:out value="${car.model}"></c:out> (<c:out value="${car.year}"></c:out>)</h5>
									  </div>
									  <ul class="card-color list-group list-group-flush">
									    <li class="card-color list-group-item">
												<div class="card-color row">
										    	<div class="card-color col-4">Ülések száma: <c:out value="${car.seats}"></c:out> </div>
										    	<div class="card-color col-4">Váltó típusa: 
										    		<c:choose>
														  <c:when test="${car.manual_gearbox}">
														    Manuális
														  </c:when>
														  <c:otherwise>
														  	Automata
														  </c:otherwise>
														</c:choose>
										    	</div>
										    	<div class="card-color col-4">Ajtók száma: <c:out value="${car.doors}"></c:out></div>
									    	</div>
									    </li>
									    <li class="card-color list-group-item">
												<div class="card-color row">
													<div class="card-color col-4">
											    	<sec:authorize access="!isAuthenticated()">
												     	<a class="btn btn-inverse-color" href="/login">Bejelentkezés</a>
		     											<a class="btn btn-inverse-color" href="/registration">Regisztráció</a>
													</sec:authorize>
											    	<sec:authorize access="isAuthenticated()">
															<form action="car" name="car">
																<object hidden="true">
																	<input name="rentFrom" value="<c:out value="${requestScope.rentFrom}"></c:out>">
																	<input name="rentTo" value="<c:out value="${requestScope.rentTo}"></c:out>">
																	<input name="fromLocation" value="<c:out value="${requestScope.fromLocation}"></c:out>">
																	<input name="toLocation" value="<c:out value="${requestScope.toLocation}"></c:out>">
																</object>
																<button name="rent" class="btn btn-inverse-color" type="submit" value="<c:out value="${car.license_id}"></c:out>">Foglalás</button>
															</form>
														</sec:authorize>
													</div>
													<div class="card-color col-4">
														Napi ár: <c:out value="${car.pricePerDay}"></c:out>
													</div>
									    	</div>
									    </li>
									  </ul>
									</div>
									<div class="card-color col-3">
								  	<img src="/resources/img/<c:out value="${car.image}"></c:out>.jpg" class="card-img" alt="...">
									</div>
							  </div>
						  </div>
					  </div>
					</c:when>
				</c:choose>
			  </c:forEach>
			  <br>
				<%@include file="/WEB-INF/layout/footer.jsp"%>
			</div>
		</div>
		<script src="/resources/js/searchbox.js"></script>
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
</body>
</html>