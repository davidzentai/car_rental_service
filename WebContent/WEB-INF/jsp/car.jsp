<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="/resources/style/style.css">
	
	<!-- jQuery is required for the datepicker -->
	<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/themes/redmond/jquery-ui.css">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
	<title>Kölcsönzés</title>
</head>
<body>
	<%@include file="/WEB-INF/layout/header.jsp"%>
	<div id="container">
		<div id="content">
  		<form:form action="rentingSuccess" name="rentingForm" onsubmit="return validateRentingForm()">
				<div class="row">
					<!--  ERROR FROM BACK-END -->
					<c:if test="${not empty error}">
						<div class="col" id="error">
							<p id="errorMessage">${error}
						</div>
					</c:if>
					<!--  ERROR FROM FRONT-END -->
				 	<div class="col" id="error" style="display:none">
				 			<p id="errorMessage">
					</div>
				</div>
				<div class="row">
					<c:choose>
				  	<c:when test="${modification}">
					  	<div class="col-6"><h1 class="main-fontcolor">Foglalás módosítása</h1></div>
						</c:when>
					  <c:otherwise>
							<div class="col-6"><h1 class="main-fontcolor">Foglalás megerősítése</h1></div>
					  </c:otherwise>
					</c:choose>
				</div>
				<div class="row">
					<div class="col empty-row"></div>
				</div>
				<div class="row">
					<div class="col-6">
						<div class="short-div"><h2><c:out value="${car.make}"></c:out> - <c:out value="${car.model}"></c:out> (<c:out value="${car.year}"></c:out>)</h2></div>
						<br>
						<div class="short-div"><h5>Ülések száma: <c:out value="${car.seats}"></c:out></h5></div>
						<div class="short-div">
							<h5>Váltó típusa: 
				    		<c:choose>
								  <c:when test="${car.manual_gearbox}">
								    Manuális
								  </c:when>
								  <c:otherwise>
								  	Automata
								  </c:otherwise>
								</c:choose>
							</h5>
						</div>
						<div class="short-div"><h5>Ajtók száma: <c:out value="${car.doors}"></c:out></h5></div>
						<br>
						<object style="display: none;">
							<input id="reservedDates" value="<c:out value="${reservedDates}"></c:out>">
						</object>
						<div class="col">
					    <div class="row">
					    	<div class="col mini-box" style="padding: 0px"><h5>Foglalás első napja:</h5></div>
							<c:choose>
					  			<c:when test="${modification}">	
   									<div class="col mini-box"><h5><input type='text' id='rentFrom' name='rentFrom' class="form-control-plaintext form" onchange="checkDate()" value="<c:out value="${rent.rent_from}"></c:out>" readonly></h5></div>
   								</c:when>
   								<c:otherwise>
   									<div class="col mini-box"><h5><input type='text' id='rentFrom' name='rentFrom' class="form-control-plaintext form" onchange="checkDate()" value="<c:out value="${requestScope.rentFrom}"></c:out>" readonly></h5></div>
   								</c:otherwise>
							</c:choose>
				    	</div>
			    	</div>
						<div class="col">
					    <div class="row">
					    	<div class="col mini-box" style="padding: 0px"><h5>Foglalás utolsó napja:</h5></div>
							<c:choose>
					  			<c:when test="${modification}">	
   									<div class="col mini-box"><h5><input type='text' id='rentTo' name='rentTo' class="form-control-plaintext form" onchange="checkDate()" value="<c:out value="${rent.rent_to}"></c:out>" readonly></h5></div>
   								</c:when>
   								<c:otherwise>
   									<div class="col mini-box"><h5><input type='text' id='rentTo' name='rentTo' class="form-control-plaintext form" onchange="checkDate()" value="<c:out value="${requestScope.rentTo}"></c:out>" readonly></h5></div>
   								</c:otherwise>
							</c:choose>					    	
				    	</div>
			    	</div>
			    	<br>
			    	<div class="col">
							<div class="row">
						    <div class="col mini-box" style="padding: 0px">
							    <select name="pickupLocation" class="form-control btn-color" >
									  <c:choose>
									  	<c:when test="${modification}">
									  		<option selected class="btn btn-color" value="<c:out value="${rent.pickup_location}"></c:out>"><c:out value="${pickupLocation}"></c:out></option>
									  	</c:when>
									  	<c:otherwise>
									    	<c:choose>
									    		<c:when test="${requestScope.fromLocation != ''}">
											  		<option selected class="btn btn-color" value="<c:out value="${requestScope.fromLocation}"></c:out>"><c:out value="${locations[fromLocation].name}"></c:out></option>
									    		</c:when>
									    		<c:otherwise>
											  		<option selected class="btn btn-color" value=''>Felvétel helye</option>
									    		</c:otherwise>
									    	</c:choose>
									  	</c:otherwise>
									  </c:choose>
								  	<c:forEach items="${requestScope.locations}" var="locations">
								    	<option class="btn-color" value="<c:out value="${locations.locationId}"></c:out>"><c:out value="${locations.name}"></c:out></option>
								    </c:forEach>
							    </select>
						    </div>
						    <div class="col mini-box">
							    <select name="dropdownLocation" class="form-control btn-color">
							    <c:choose>
									  	<c:when test="${modification}">
									  		<option selected class="btn btn-color" value="<c:out value="${rent.dropdown_location}"></c:out>"><c:out value="${dropdownLocation}"></c:out></option>
									  	</c:when>
									  	<c:otherwise>
									    	<c:choose>
									    		<c:when test="${requestScope.fromLocation != ''}">
											  		<option selected class="btn btn-color" value="<c:out value="${requestScope.toLocation}"></c:out>"><c:out value="${locations[toLocation].name}"></c:out></option>
									    		</c:when>
									    		<c:otherwise>
											  		<option selected class="btn btn-color" value=''>Leadás helye</option>
									    		</c:otherwise>
									    	</c:choose>
									  	</c:otherwise>
									  </c:choose>
								  	<c:forEach items="${requestScope.locations}" var="locations">
								    	<option class="btn-color" value="<c:out value="${locations.locationId}"></c:out>"><c:out value="${locations.name}"></c:out></option>
								    </c:forEach>
							    </select>
								</div>
					    </div>
				   	</div>
				   	<br>
				   	<div class="col">
							<div class="row">
								<div class="col mini-box" style="padding: 0px">
									<object style="display: none;">
										<input id="oneDayPrice" value="<c:out value="${car.pricePerDay}"></c:out>">
									</object>
									<div class="short-div">
										<h5>
											Végösszeg: <div id="totalPrice" class="main-fontcolor" style="text-align: center;"></div>
										</h5>
									</div>
								</div>
								<div class="col mini-box">
									<object style="display: none;">
										<input name="email" value="<c:out value="${pageContext.request.userPrincipal.name}"></c:out>">
									</object>
									<c:choose>
								  		<c:when test="${modification}">
									  		<button name="confirmModification" class="btn btn-color btn-maxlen" type="submit" onclick="confirmModification" value="<c:out value="${rent.renting_id}"></c:out>">Módosítás</button>
										</c:when>
									  <c:otherwise>
											<button name="confirmRenting" class="btn btn-color btn-maxlen" type="submit" onclick="rentingSuccess" value="<c:out value="${car.license_id}"></c:out>">Foglalás</button>
									  </c:otherwise>
									</c:choose>
								</div>
							</div>
						</div>
					</div>
					<div class="col-6"><img class="img-border" src="/resources/img/<c:out value="${car.image}"></c:out>.jpg" class="card-img" alt="..."></div>
				</div>
			</form:form>
		</div>
		<%@include file="/WEB-INF/layout/footer.jsp"%>
	</div>
	<script src="/resources/js/searchbox.js"></script>
</body>
</html>