<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="true"%>
<%@ page import="java.util.Date" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="/resources/style/style.css">
	<title>Autók kezelése</title>
</head>
<body>
	<%@include file="/WEB-INF/layout/header.jsp"%>
	<div id="container">
		<div id="content">
			<!--  MESSAGE FROM BACK-END -->		
			<c:if test="${not empty msg}">
				<div class="col" id="msg">${msg}</div>
			</c:if>
			<%-- <c:if test="${not empty error}">
				<div class="col" id="msg">
					<p id="errorMessage">${error}
				</div>
			</c:if> --%>
				<table class="table">
				  <thead>
				    <tr class="main-fontcolor">
				      <th scope="col">#</th>
				      <th scope="col">Rendszám</th>
				      <th scope="col">Autó</th>
				      <th scope="col">Profilkép</th>
				      <th scope="col">Napi ár</th>
				      <th scope="col">Elérhető</th>
				      <th scope="col"></th>
				      <th scope="col"></th>
				    </tr>
				  </thead>
				  <tbody>
				  	<c:set var="rownumber" value="1" scope="page"></c:set>
				  	<c:forEach items="${requestScope.cars}" var="car">
					    <tr>
					    	<form:form action="modifyCar">
						      <th><c:out value="${rownumber}"></c:out></th>
						      <td><c:out value="${car.license_id}"></c:out></td>
						      <td><c:out value="${car.make}"></c:out> - <c:out value="${car.model}"></c:out> (<c:out value="${car.year}"></c:out>)</td>
							  <object style="display: none;">
							  	<img class="img-border img-preview" id="preview">
								<input id="imageFile" name="imageFile" value="">
							  </object>
						      <td><input class="list-input" id="image" name="image" type="file" onchange="uploadImage(this)" onclick="changeButtonColor(${rownumber})" value="<c:out value="${car.image}"></c:out>"></td>
						      <%-- <td><input class="list-input" id="image" name="image" type="text" onchange="changeButtonColor(${rownumber})" value="<c:out value="${car.image}"></c:out>"></td> --%>
						      <td><input class="list-input" id="price" name="price" type="number" onchange="changeButtonColor(${rownumber})" value="<c:out value="${car.pricePerDay}"></c:out>"></td>
						      <td style="padding: 3vh"><input class="list-checkbox" id="available" name="available" type="checkbox" onchange="changeButtonColor(${rownumber})" value="<c:out value="${car.available}"></c:out>"></td>
									<td><button class="list-button" id="save" name="save" type="submit"  value="<c:out value="${car.license_id}"></c:out>"><img alt="save" src="/resources/img/save-icon.png"></button></td>
								</form:form>	
					      <form:form action="deleteCar">
				      		<td><button class="list-button" id="delete" name="delete" type="submit" value="<c:out value="${car.license_id}"></c:out>"><img alt="delete" src="/resources/img/delete-icon.png"></button></td>
					      </form:form>
					    </tr>
				  		<c:set var="rownumber" value="${rownumber + 1}" scope="page"></c:set>
				    </c:forEach>
				  </tbody>
				</table>	
		</div>
		<%@include file="/WEB-INF/layout/footer.jsp"%>
	</div>
	<script src="/resources/js/admin.js"></script>
</body>
</html>