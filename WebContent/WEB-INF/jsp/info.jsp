<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page session="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="/resources/style/style.css">
	<title>Szabályzat</title>
</head>
<body>
	<%@include file="/WEB-INF/layout/header.jsp"%>
	<div id="container">
		<div id="content">
			<h1>Kölcsönzés feltételei</h1>
			<ul>
				<li>Csak regisztrál felhasználók kölcsönözhetnek autót</li>
				<li>A kölcsönzést a szolgáltató adott esetben felmondhatja. Erről mindenképp kap az ügyfél tájékoztatást.</li>
				<li>A kölcsönzés csak akkor hozható létre, ha:
					<ul>
						<li>az kezdő- és végdátuma között nincs másik foglalás</li>
						<li>a kezdő- és végdátum napján nem foglalt az autó</li>
						<li>a kiválasztott autóra leadni kívánt igényünket megelőző foglalás (ha van) leadási helye megegyezik az általunk kiválasztott felvételi telephellyel</li>
						<li>a kiválasztott autóra leadni kívánt igényünket követő foglalás (ha van) felvételi helye megegyezik az általunk kiválasztott leadási telephellyel</li>
						<li>a kiválasztott leadási telephely nincs megtelve</li>
					</ul>
				</li>
			</ul>
		</div>
		<%@include file="/WEB-INF/layout/footer.jsp"%>
	</div>
</body>
</html>