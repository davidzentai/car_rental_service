<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="/resources/style/style.css">
<!-- <nav class="navbar sticky-top navbar-expand-lg navbar-light bg-info"> -->
<nav class="navbar sticky-top navbar-expand-lg header">
  <a class="navbar-brand" href="/">
    <img src="https://picsum.photos/20" width="30" height="30" class="d-inline-block align-top" alt="">
    Car Rental Service
  </a>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav ml-auto">
      <!-- <a class="nav-item nav-link" data-target="#loginModal" data-toggle="modal" onclick="checkLoginForm()" href="#">Bejelentkezés</a> -->
      <a class="nav-item nav-link" href="/login">Bejelentkezés</a>
      <!-- <a class="nav-item nav-link" data-target="#registrationModal" data-toggle="modal" onclick="checkRegForm()" href="#">Regisztráció</a> -->
      <a class="nav-item nav-link" href="/registration">Regisztráció</a>
      <!-- <a class="nav-item nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a> -->
    </div>
  </div>
</nav>

<!-- Login Modal -->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModelLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    	<form name="loginForm" action="LoginController" method='POST'>
	      <div class="modal-header">
	        <h5 class="modal-title" id="loginModelLabel">Bejelentkezés</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <input type="text" class="form-control-plaintext" name="email" placeholder="example@server.dom" onKeyup="checkLoginForm()" style="background: #f0faff;">
	        <br>
	        <input type="password" class="form-control-plaintext" name="password" placeholder="password" onKeyup="checkLoginForm()" style="background: #f0faff;">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Mégse</button>
	        <button id="submitLogin" type="submit" name="currentURL" value=${requestScope['javax.servlet.forward.request_uri']} class="btn btn-primary">Belépés</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<!-- Registration Modal -->
<div class="modal fade" id="registrationModal" tabindex="-1" role="dialog" aria-labelledby="registrationModelLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
	   	<form name="regForm" action="RegistrationController" method="post">
	      <div class="modal-header">
	        <h5 class="modal-title" id="registrationModelLabel">Regisztráció</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<input type="text" class="form-control-plaintext" name="name" placeholder="Név: Teszt Elek" onKeyup="checkRegForm()" style="background: #f0faff;" />
	      	<!-- <p class="warning" id="nameWarn">A név mező kitöltése kötelező -->
	      	<br>
	      	<input type="text" class="form-control-plaintext" name="phone" placeholder="Telefon: +36200123456" onKeyup="checkRegForm()" style="background: #f0faff;" />
	      	<br>
	      	<input type="email" class="form-control-plaintext" name="email" placeholder="E-mail: example@server.dom" onKeyup="checkRegForm()" style="background: #f0faff;" />
	      	<br>
	        <input type="password" class="form-control-plaintext" name="password" placeholder="Jelszó: password" onKeyup="checkRegForm()" style="background: #f0faff;" />
	      	<!-- <br>
	        <input type="password" class="form-control-plaintext" name="confirmPassword" placeholder="Jelszó újra: password" style="background: #f0faff;"> -->
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Mégse</button>
	        <button id="submitReg" type="submit" name="currentURL" value=${requestScope['javax.servlet.forward.request_uri']} class="btn btn-primary">Regisztráció</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<script src="/resources/js/checkForms.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</html>