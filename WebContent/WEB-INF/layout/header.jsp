<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- <html lang="en" xmlns:layout="http://www.ultraq.net.nz/thymeleaf/layout" xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity4" layout:decoration="default-layout"> -->
<html>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="/resources/style/style.css">
<!-- <nav class="navbar sticky-top navbar-expand-lg navbar-light bg-info"> -->
<nav class="navbar sticky-top navbar-expand-lg header">
  <a class="navbar-brand" href="/index">
    <img src="/resources/img/logo.png" width="30" height="30" class="d-inline-block align-top" alt="">
    Car Rental Service
  </a>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <ul class="navbar-nav ml-auto">
    	<!-- UNAUTHORIZED -->
    	<sec:authorize access="!isAuthenticated()">
	     	<a class="nav-item nav-link" href="/login">Bejelentkezés</a>
	     	<a class="nav-item nav-link" href="/registration">Regisztráció</a>
			</sec:authorize>
      <!--  LOGGED IN USER-->
      <!--  LOGGED IN ADMIN -->
      <sec:authorize access="hasRole('ADMIN')">
      	<a class="nav-item nav-link" href="/adminCarList">Autók kezelése</a>
      	<a class="nav-item nav-link" href="/adminNewCar">Új autó</a>
      	<a class="nav-item nav-link" href="/adminRentList">Foglalások kezelése</a>
			</sec:authorize>
      <sec:authorize access="hasRole('USER')">
      	<a class="nav-item nav-link" href="/index">Kölcsönzés</a>
      	<a class="nav-item nav-link" href="/carhistory">Rendeléseim</a>
      </sec:authorize>
    	<sec:authorize access="isAuthenticated()">
	      <a class="nav-item nav-link" href="javascript:logoutSubmit()">Kijelentkezés</a>
			</sec:authorize>
    </ul>
  </div>
</nav>

<c:url value="/j_spring_security_logout" var="logoutUrl" />
<!-- csrt for log out-->
<form action="${logoutUrl}" method="post" id="logoutForm">
  <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>

<script src="/resources/js/userManagement.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</html>

